/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stm32l071.h"
#include "sim800l.h"
#include "l86.h"
#include "ina219.h"
#include "string.h"
#include "stdio.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct
{
	_Bool rtc_sync;
	_Bool payload_ready;
	_Bool send_status;
	_Bool sleep_time;
} sync_signal_t;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define payload_len 110
#define mqttpacket_len 200

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
_Bool Start_Main_task = 0;
sync_signal_t sync_signal = { 0 };
uint8_t payload[payload_len] = { 0 };
uint8_t mqtt_packet[mqttpacket_len] = { 0 };

extern RTC_HandleTypeDef hrtc;
extern IWDG_HandleTypeDef hiwdg;

/* USER CODE END Variables */
osThreadId mainTaskHandle;
osThreadId sim800lrxTaskHandle;
osThreadId l86rxTaskHandle;
osThreadId rtcsyncTaskHandle;
osThreadId sysinitTaskHandle;
osMutexId nmeaMutexHandle;
osMutexId alarmconfigMutexHandle;
osMutexId l86rxMutexHandle;
osMutexId uart1MutexHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartMainTask(void const * argument);
void StartSim800lrxTask(void const * argument);
void StartL86rxTask(void const * argument);
void StartRTCsyncTask(void const * argument);
void StartSysinitTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Create the mutex(es) */
  /* definition and creation of nmeaMutex */
  osMutexDef(nmeaMutex);
  nmeaMutexHandle = osMutexCreate(osMutex(nmeaMutex));

  /* definition and creation of alarmconfigMutex */
  osMutexDef(alarmconfigMutex);
  alarmconfigMutexHandle = osMutexCreate(osMutex(alarmconfigMutex));

  /* definition and creation of l86rxMutex */
  osMutexDef(l86rxMutex);
  l86rxMutexHandle = osMutexCreate(osMutex(l86rxMutex));

  /* definition and creation of uart1Mutex */
  osMutexDef(uart1Mutex);
  uart1MutexHandle = osMutexCreate(osMutex(uart1Mutex));

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of mainTask */
  osThreadDef(mainTask, StartMainTask, osPriorityNormal, 0, 256);
  mainTaskHandle = osThreadCreate(osThread(mainTask), NULL);

  /* definition and creation of sim800lrxTask */
  osThreadDef(sim800lrxTask, StartSim800lrxTask, osPriorityNormal, 0, 384);
  sim800lrxTaskHandle = osThreadCreate(osThread(sim800lrxTask), NULL);

  /* definition and creation of l86rxTask */
  osThreadDef(l86rxTask, StartL86rxTask, osPriorityNormal, 0, 384);
  l86rxTaskHandle = osThreadCreate(osThread(l86rxTask), NULL);

  /* definition and creation of rtcsyncTask */
  osThreadDef(rtcsyncTask, StartRTCsyncTask, osPriorityNormal, 0, 128);
  rtcsyncTaskHandle = osThreadCreate(osThread(rtcsyncTask), NULL);

  /* definition and creation of sysinitTask */
  osThreadDef(sysinitTask, StartSysinitTask, osPriorityNormal, 0, 384);
  sysinitTaskHandle = osThreadCreate(osThread(sysinitTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */

  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartMainTask */
/**
  * @brief  Function implementing the mainTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartMainTask */
void StartMainTask(void const * argument)
{
  /* USER CODE BEGIN StartMainTask */
	while(Start_Main_task != 1)
	{
		osThreadYield();
	}
	uint32_t time_to_generatePayload = HAL_GetTick();
	/* Infinite loop */
	for (;;)
	{
		if((HAL_GetTick() - time_to_generatePayload) > 1000)
		{
			uint8_t mqqt_packet_len = 0;
			while (1)
			{
				if (osMutexWait(nmeaMutexHandle, osWaitForever) == osOK)
				{
					stm32l071_create_json(payload);
					osMutexRelease(nmeaMutexHandle);
					break;
				}
				osThreadYield();
			}
			mqqt_packet_len = stm32l071_create_mqttpacket(&payload[0], &mqtt_packet[0], mqttpacket_len);
			sync_signal.send_status = 0;
			if((sim800l_tcpstat_read() == 0) && (mqqt_packet_len != 0))
			{
				time_to_generatePayload = HAL_GetTick();
				while((HAL_GetTick() - time_to_generatePayload) < 2000)
				{
					if (osMutexWait(uart1MutexHandle, osWaitForever) == osOK)
					{
						HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
						sync_signal.send_status = sim800l_cipsendData((const char*) &mqtt_packet[0], mqqt_packet_len);
						if(sync_signal.send_status == 1)
						{
							stm32l071_set_mqttsession(1);
						}
						else
						{
							stm32l071_set_mqttsession(0);
						}
						HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
						osMutexRelease(uart1MutexHandle);
						break;
					}
					osThreadYield();
				}
			}
			if((sync_signal.send_status == 0) && (mqqt_packet_len != 0))
			{
				// Log to Flash
			}
			memset((void*) &payload[0], 0x00, payload_len);
			memset((void*) &mqtt_packet[0], 0x00, mqttpacket_len);
			time_to_generatePayload = HAL_GetTick();
		}
		osThreadYield();
	}
  /* USER CODE END StartMainTask */
}

/* USER CODE BEGIN Header_StartSim800lrxTask */
/**
* @brief Function implementing the sim800lrxTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartSim800lrxTask */
void StartSim800lrxTask(void const * argument)
{
  /* USER CODE BEGIN StartSim800lrxTask */
	/* Infinite loop */
	for (;;)
	{
		if (sim800l_data_available() > 0)
		{
			sim800l_to_2ndbuff();
		}
		else
		{
			sim800l_2ndbuff_process();
		}
		osThreadYield();
	}
  /* USER CODE END StartSim800lrxTask */
}

/* USER CODE BEGIN Header_StartL86rxTask */
/**
* @brief Function implementing the l86rxTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartL86rxTask */
void StartL86rxTask(void const * argument)
{
  /* USER CODE BEGIN StartL86rxTask */
	uint32_t Refresh_IWDG = 0;
	/* Infinite loop */
	for (;;)
	{
		if (l86_data_available() > 0)
		{
			if (l86_rmc_selection())
			{
				while (1)
				{
					if (osMutexWait(nmeaMutexHandle, osWaitForever) == osOK)
					{
						HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET);
						l86_nmea_parser();
						HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
						osMutexRelease(nmeaMutexHandle);
						break;
					}
					osThreadYield();
				}
			}
		}
		if((HAL_GetTick() - Refresh_IWDG) > 2000)
		{
			HAL_IWDG_Refresh(&hiwdg);
		}
		osThreadYield();
	}
  /* USER CODE END StartL86rxTask */
}

/* USER CODE BEGIN Header_StartRTCsyncTask */
/**
* @brief Function implementing the rtcsyncTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartRTCsyncTask */
void StartRTCsyncTask(void const * argument)
{
  /* USER CODE BEGIN StartRTCsyncTask */
	/* Infinite loop */
	for(;;)
	{
		if ((l86_get_rmcStat() == 1) && (sync_signal.rtc_sync == 0))
		{
			while (1)
			{
				if (osMutexWait(nmeaMutexHandle, osWaitForever) == osOK)
				{
					stm32l071_set_RTCTimeDate(1);
					sync_signal.rtc_sync = 1;
					osMutexRelease(nmeaMutexHandle);
					break;
				}
				osThreadYield();
			}
		}
		else if ((sim800l_ntpSync_read() == 1) && (sync_signal.rtc_sync == 0))
		{
			while(1)
			{
				if(osMutexWait(uart1MutexHandle, osWaitForever) == osOK)
				{
					stm32l071_set_RTCTimeDate(0);
					sync_signal.rtc_sync = 1;
					osMutexRelease(uart1MutexHandle);
					break;
				}
				osThreadYield();
			}
		}
		if(sync_signal.rtc_sync == 1)
		{
			while(1)
			{
				if(osMutexWait(uart1MutexHandle, osWaitForever) == osOK)
				{
					while (1)
					{
						if (osMutexWait(nmeaMutexHandle, osWaitForever) == osOK)
						{
							SET_BIT(RTC->BKP0R, 0x01);
							stm32l071_check_ophour();
							sync_signal.rtc_sync = 1;
							osMutexRelease(nmeaMutexHandle);
							osMutexRelease(uart1MutexHandle);
							break;
						}
						osThreadYield();
					}
					break;
				}
				osThreadYield();
			}
			osThreadTerminate(rtcsyncTaskHandle);
		}
		osThreadYield();
	}
  /* USER CODE END StartRTCsyncTask */
}

/* USER CODE BEGIN Header_StartSysinitTask */
/**
* @brief Function implementing the sysinitTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartSysinitTask */
void StartSysinitTask(void const * argument)
{
  /* USER CODE BEGIN StartSysinitTask */
	if(stm32l071_read_backupreg() == 1)
	{
		stm32l071_check_ophour();
	}
	while(1)
	{
		if(osMutexWait(nmeaMutexHandle, osWaitForever) == osOK)
		{
			l86_init();
			l86_start_rx();
			osMutexRelease(nmeaMutexHandle);
			break;
		}
		osThreadYield();
	}
	ina219_init();
	stm32l071_init_tempSensor();
	sim800l_startup();
	while(1)
	{
		if(osMutexWait(uart1MutexHandle, osWaitForever) == osOK)
		{
		sim800l_start_rx();
		sim800l_init();
		osMutexRelease(uart1MutexHandle);
		break;
		}
		osThreadYield();
	}
	Start_Main_task = 1;
	/* Infinite loop */
	for (;;)
	{
		if (sim800l_errorcode_read() != INIT_OK)
		{
			while(1)
			{
				if(osMutexWait(uart1MutexHandle, osWaitForever) == osOK)
				{
					sim800l_init();
					osMutexRelease(uart1MutexHandle);
					break;
				}
				osThreadYield();
			}
		}
		if((sim800l_ntpSync_read() == 0) && (sync_signal.rtc_sync == 0) && (sim800l_tcpstat_read() == 0))
		{
			while(1)
			{
				if(osMutexWait(uart1MutexHandle, osWaitForever) == osOK)
				{
					sim800l_ntpSync();
					osMutexRelease(uart1MutexHandle);
					break;
				}
				osThreadYield();
			}
		}
		if (sim800l_tcpstat_read() == 2)
		{
			while(1)
			{
				if(osMutexWait(uart1MutexHandle, osWaitForever) == osOK)
				{
					stm32l071_set_mqttsession(0);
					sim800l_tcp_reconnect();
					osMutexRelease(uart1MutexHandle);
					break;
				}
				osThreadYield();
			}
		}
		if (sim800l_tcpstat_read() == 3)
		{
			while(1)
			{
				if(osMutexWait(uart1MutexHandle, osWaitForever) == osOK)
				{
					stm32l071_set_mqttsession(0);
					sim800l_pdpdeact_react();
					osMutexRelease(uart1MutexHandle);
					break;
				}
				osThreadYield();
			}
		}
		if(sync_signal.sleep_time == 1)
		{
			while(1)
			{
				if(osMutexWait(uart1MutexHandle, osWaitForever) == osOK)
				{
					while (1)
					{
						if (osMutexWait(nmeaMutexHandle, osWaitForever) == osOK)
						{
							stm32l071_check_ophour();
							osMutexRelease(nmeaMutexHandle);
							osMutexRelease(uart1MutexHandle);
							break;
						}
						osThreadYield();
					}
					break;
				}
				osThreadYield();
			}
		}
		osThreadYield();
	}
  /* USER CODE END StartSysinitTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
void check_rtc(void)
{
	RTC_TimeTypeDef sTime = {0};
	RTC_DateTypeDef sDate = {0};
	RTC_AlarmTypeDef sAlarm = {0};

	HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
	HAL_RTC_GetAlarm(&hrtc, &sAlarm, RTC_ALARM_A, RTC_FORMAT_BIN);
	HAL_Delay(1);
}

void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{
	check_rtc();
	if(sync_signal.rtc_sync == 1)
	{
		sync_signal.sleep_time = 1;
	}
}

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
