#include "l86.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"

#define gps_int_rx_buff_size 384
#define gps_rx_buff_size 384

extern UART_HandleTypeDef huart2;

typedef struct
{
	uint8_t rxinit_status;
	uint8_t error_code;
} l86_init_data_t;

uint8_t gps_int_rx_buff[gps_int_rx_buff_size];
uint8_t gps_rx_buff[gps_rx_buff_size];
uint8_t gps_rx_data;
l86_init_data_t l86_init_data = { 0 };
parse_l86_data_t gps_parsed_data = { 0 };
volatile uint8_t gps_rx_buff_head;
volatile uint8_t gps_rx_buff_tail;

int16_t l86_read(void);
uint8_t l86_get_checksum(void);
uint8_t l86_nmea_checksum(void);
uint8_t l86_nmea_comma_counter(void);
float l86_degmin_to_deg(float* degmin);
void l86_normalize_date(parse_l86_data_t* parsed_l86_data);
void l86_uart_change(void);
_Bool l86_sar_command(const char* command, const char* reply);

_Bool l86_sar_command(const char* command, const char* reply)
{
	uint8_t init_reply_buff[50] = { 0 };
	uint8_t command_len = strlen(command);
	uint8_t reply_len = strlen(reply);
	__HAL_UART_CLEAR_FLAG(&huart2, UART_CLEAR_OREF);
	__HAL_UART_FLUSH_DRREGISTER(&huart2);
	HAL_UART_Transmit(&huart2, (uint8_t*) command, command_len, command_len);
	HAL_UART_Receive(&huart2, &init_reply_buff[0], reply_len, 1000);
	if((strcmp((const char*) &init_reply_buff[0], reply) == 0) || (reply == 0x00))
	{
		memset(&init_reply_buff[0], 0x00, 50);
		return 1;
	}
	memset(&init_reply_buff[0], 0x00, 50);
	return 0;
}

void l86_uart_change(void)
{
	HAL_UART_DeInit(&huart2);
	huart2.Init.BaudRate = 115200;
	HAL_UART_Init(&huart2);
}

uint8_t l86_read_errorcode(void)
{
	return l86_init_data.error_code;
}

void l86_init(void)
{
	l86_init_data.error_code = 0;
	if(l86_sar_command("$PMTK314,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n\0", 0x00))
	{
		l86_init_data.error_code &= ~(DISABLE_NMEA_OUTPUT);
	}
	else
	{
		l86_init_data.error_code |= DISABLE_NMEA_OUTPUT;
	}
	HAL_Delay(500);
	if(l86_sar_command("$PQTXT,W,0,1*23\r\n\0",0x00))
	{
		l86_init_data.error_code &= ~(DISABLE_GPTXT);
	}
	else
	{
		l86_init_data.error_code |= DISABLE_GPTXT;
	}
	HAL_Delay(500);
	if(l86_sar_command("$PMTK251,115200*1F\r\n\0", 0x00))
	{
		l86_init_data.error_code &= ~(CHANGE_BAUD_115200);
	}
	else
	{
		l86_init_data.error_code |= CHANGE_BAUD_115200;
	}
	HAL_Delay(500);
	l86_uart_change();
	HAL_Delay(500);
	if(l86_sar_command("$PMTK220,1000*1F\r\n\0", "$PMTK001,220,3,1000*1D\r\n"))
	{
		l86_init_data.error_code &= ~(POS_FIX_INTERVAL);
	}
	else
	{
		l86_init_data.error_code |= POS_FIX_INTERVAL;
	}
	HAL_Delay(500);
	if(l86_sar_command("$PMTK301,2*2E\r\n\0", "$PMTK001,301,3*32\r\n"))
	{
		l86_init_data.error_code &= ~(DGPS_MODE);
	}
	else
	{
		l86_init_data.error_code |= DGPS_MODE;
	}
	HAL_Delay(500);
	if(l86_sar_command("$PMTK313,1*2E\r\n\0", "$PMTK001,313,3*31\r\n"))
	{
		l86_init_data.error_code &= ~(SBAS_ENABLE);
	}
	else
	{
		l86_init_data.error_code |= SBAS_ENABLE;
	}
	HAL_Delay(500);
	if(l86_sar_command("$PMTK353,1,1,0,0,0*2B\r\n\0", "$PMTK001,353,3,1,1,0,0,0,3*36\r\n"))
	{
		l86_init_data.error_code &= ~(SEARCH_MODE);
	}
	else
	{
		l86_init_data.error_code |= SEARCH_MODE;
	}
	HAL_Delay(500);
	if(l86_sar_command("$PMTK386,1*22\r\n\0", "$PMTK001,386,3*3D\r\n"))
	{
		l86_init_data.error_code &= ~(SPEED_THRS);
	}
	else
	{
		l86_init_data.error_code |= SPEED_THRS;
	}
	HAL_Delay(500);
	if(l86_sar_command("$PMTK869,1,1*35\r\n\0", "$PMTK001,869,3*37\r\n"))
	{
		l86_init_data.error_code &= ~(EASY_ENABLE);
	}
	else
	{
		l86_init_data.error_code |= EASY_ENABLE;
	}
	HAL_Delay(500);
	if(l86_sar_command("$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n\0", "$PMTK001,314,3*36\r\n"))
	{
		l86_init_data.error_code &= ~(RMC_OUTPUT);
	}
	else
	{
		l86_init_data.error_code |= RMC_OUTPUT;
	}
}

void l86_reboot(void)
{
	HAL_GPIO_WritePin(GPSRST_GPIO_Port, GPSRST_Pin, GPIO_PIN_RESET);
	HAL_Delay(1000);
	HAL_GPIO_WritePin(EN3V3_swGPS_GPIO_Port, EN3V3_swGPS_Pin, GPIO_PIN_RESET);
	HAL_Delay(1000);
	HAL_GPIO_WritePin(GPSRST_GPIO_Port, GPSRST_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(EN3V3_swGPS_GPIO_Port, EN3V3_swGPS_Pin, GPIO_PIN_SET);
	HAL_Delay(2000);
}

void l86_start_rx(void)
{
	l86_init_data.rxinit_status = HAL_UART_Receive_IT(&huart2, &gps_rx_data, 1);
}

uint8_t l86_read_rxStat(void)
{
	return l86_init_data.rxinit_status;
}

void l86_rx_handler(void)
{
	uint8_t rx_index = (gps_rx_buff_head + 1) % gps_int_rx_buff_size;
	if(rx_index != gps_rx_buff_tail)
	{
		gps_int_rx_buff[gps_rx_buff_head] = gps_rx_data;
		gps_rx_buff_head = rx_index;
	}
}

uint8_t l86_data_available(void)
{
	uint8_t available = ((gps_int_rx_buff_size + gps_rx_buff_head - gps_rx_buff_tail) % gps_int_rx_buff_size);
	return available;
}

int16_t l86_read(void)
{
	if(gps_rx_buff_head == gps_rx_buff_tail)
	{
		return -1;
	}
	else
	{
		uint8_t c = gps_int_rx_buff[gps_rx_buff_tail];
		gps_rx_buff_tail = (gps_rx_buff_tail + 1) % gps_int_rx_buff_size;
		return (uint8_t) c;
	}
}

_Bool l86_rmc_selection(void)
{
	int16_t gps_char = l86_read();
	if(gps_char != -1)
	{
		static _Bool nmea_char_side = 0;
		static _Bool nmea_rmc = 0;
		static _Bool nmea_lf = 0;
		static uint8_t nmea_char_count = 0;
		static uint8_t nmea_sentence[7] = {0};
		switch (gps_char)
		{
			case '$' :
				nmea_char_side = 1;
				nmea_rmc = 0;
				nmea_char_count = 0;
				break;
			case '\r':
				if(nmea_rmc)
				{
					nmea_char_side = 0;
					nmea_lf = 1;
				}
				break;
			default:
				break;
		}
		if(nmea_char_side)
		{
			if(nmea_char_count <= 5)
			{
				nmea_sentence[nmea_char_count] = gps_char;
			}
			if(nmea_char_count == 5)
			{
				if(strstr((const char*) &nmea_sentence[0], "RMC") != NULL)
				{
					nmea_rmc = 1;
					memcpy(&gps_rx_buff[0], &nmea_sentence[0], 6);
					memset(&nmea_sentence[0], 0x00, 7);
				}
				else
				{
					nmea_char_side	= 0;
					nmea_char_count = 0;
				}
			}
			if(nmea_rmc)
			{
				gps_rx_buff[nmea_char_count] = gps_char;
			}
			nmea_char_count++;
		}
		if(nmea_lf)
		{
			nmea_lf = 0;
			return 1;
		}
	}
	return 0;
}

uint8_t l86_nmea_comma_counter(void)
{
	uint8_t* cursor = &gps_rx_buff[0];
	uint8_t commas = 0;
	do
	{
		if(*cursor == ',')
		{
			commas++;
		}
		cursor++;
	} while(*cursor != 0x00);
	return commas;
}

uint8_t l86_nmea_checksum(void)
{
	uint8_t* cursor = &gps_rx_buff[1];
	uint8_t self_checksum = 0;
	do
	{
		self_checksum ^= *cursor;
		cursor++;
	} while(*cursor != '*');
	return self_checksum;
}

uint8_t l86_get_checksum(void)
{
	uint8_t* cursor = &gps_rx_buff[strlen((const char*) &gps_rx_buff[0]) - 2];
	return strtol((const char*) cursor, NULL, 16);
}

float l86_degmin_to_deg(float* degmin)
{
	float separate_deg = *degmin / 100.0f;
	float deg = (uint8_t) separate_deg;
	float min_to_deg = (separate_deg - deg) / 0.60f;
	deg += min_to_deg;
	return deg;
}

void l86_nmea_parser(void)
{
	parse_l86_data_t temp_buffer = { 0 };
	temp_buffer.chksum = l86_nmea_checksum();
	if(l86_nmea_comma_counter() == 13 && l86_get_checksum() == temp_buffer.chksum)
	{
		uint8_t* cursor = &gps_rx_buff[0];
		uint8_t rmc_element[15] = {0};
		uint8_t commas = 0;
		int char_elapsed = 0;
		if(*cursor == '$')
		{
			while (sscanf((const char*) cursor, "%15[^,]%n", &rmc_element[0], &char_elapsed) >= 0)
			{
				switch(commas)
				{
					case 1 :
						temp_buffer.time = atoi((const char*) &rmc_element[0]);
						if(temp_buffer.time > 235959)
						{
							temp_buffer.time = -1;
						}
						temp_buffer.hh = (uint8_t) (temp_buffer.time / 10000);
						temp_buffer.mi = (uint8_t) ((uint8_t) (temp_buffer.time / 100) - (temp_buffer.hh * 100));
						temp_buffer.ss = (uint8_t) (temp_buffer.time - ((temp_buffer.hh * 10000) + (temp_buffer.mi * 100)));
						memset(&rmc_element[0], 0x00, 15);
						break;
					case 2 :
						if(rmc_element[0] == 'A')
						{
							temp_buffer.rmc = 1;
						}
						else
						{
							temp_buffer.rmc = 0;
						}
						memset(&rmc_element[0], 0x00, 15);
						break;
					case 3 :
						temp_buffer.lat = atof((const char*) &rmc_element[0]);
						temp_buffer.lat = l86_degmin_to_deg(&temp_buffer.lat);
						if(temp_buffer.lat >= 12.0 && temp_buffer.lat <= 5.0)
						{
							temp_buffer.lat = 13.0;
						}
						memset(&rmc_element[0], 0x00, 15);
						break;
					case 4 :
						if(rmc_element[0] == 'S')
						{
							temp_buffer.lat *= (-1.0);
						}
						else if(rmc_element[0] == 'N')
						{
							temp_buffer.lat *= 1.0;
						}
						memset(&rmc_element[0], 0x00, 15);
						break;
					case 5 :
						temp_buffer.lon = atof((const char*) &rmc_element[0]);
						temp_buffer.lon = l86_degmin_to_deg(&temp_buffer.lon);
						if(temp_buffer.lon >= 141.0 && temp_buffer.lon <= 95.0)
						{
							temp_buffer.lon = 13.0;
						}
						memset(&rmc_element[0], 0x00, 15);
						break;
					case 6 :
						if(rmc_element[0] == 'W')
						{
							temp_buffer.lon *= (-1.0);
						}
						else if(rmc_element[0] == 'E')
						{
							temp_buffer.lon *= 1.0;
						}
						memset(&rmc_element[0], 0x00, 15);
						break;
					case 7 :
						temp_buffer.spd = ((atof((const char*) &rmc_element[0]))*1.852);
						if(temp_buffer.spd >=300 && temp_buffer.spd <= 0)
						{
							temp_buffer.spd = -1;
						}
						memset(&rmc_element[0], 0x00, 15);
						break;
					case 8 :
						temp_buffer.heading = atof((const char*) &rmc_element[0]);
						if(temp_buffer.heading > 360 && temp_buffer.heading < 0)
						{
							temp_buffer.heading = -1;
						}
						memset(&rmc_element[0], 0x00, 15);
						break;
					case 9 :
						temp_buffer.date = atoi((const char*) &rmc_element[0]);
						if(temp_buffer.date >= 311299 && temp_buffer.date <= 0)
						{
							temp_buffer.date = -1;
						}
						temp_buffer.dd = (uint8_t) (temp_buffer.date / 10000);
						temp_buffer.mo = (uint8_t) (((uint8_t) (temp_buffer.date / 100)) - (temp_buffer.dd * 100));
						temp_buffer.yy = (uint8_t) (temp_buffer.date - ((temp_buffer.dd * 10000) + (temp_buffer.mo * 100)));
						if(temp_buffer.hh >= 17)
						{
							temp_buffer.hh -= 17;
							l86_normalize_date(&temp_buffer);
						}
						else
						{
							temp_buffer.hh += 7;
						}
						memset(&rmc_element[0], 0x00, 15);
						break;
					default:
						break;
				}
				cursor += char_elapsed;
				do
				{
					cursor ++;
					commas ++;
				} while(*cursor == ',');
				if(*cursor == 0x00) break;
				if(commas > 11)
				{
					cursor += 2;
				}
			}
		}
	}
	gps_parsed_data = temp_buffer;
	memset((void*) &gps_rx_buff[0], 0x00, gps_rx_buff_size);
}

parse_l86_data_t l86_get_gpsData(void)
{
	return gps_parsed_data;
}

_Bool l86_get_rmcStat(void)
{
	return gps_parsed_data.rmc;
}

_Bool l86_leap_year_check(uint8_t yy)
{
    uint16_t year = yy + 2000;
    if((year % 4) == 0)
    {
        if((year % 100) == 0)
        {
            if((year % 400) == 0) return 1;
            else return 0;
        }
        else return 1;
    }
    else return 0;
}

void l86_normalize_date(parse_l86_data_t* parsed_l86_data)
{
	if((parsed_l86_data->mo == 1U) || (parsed_l86_data->mo == 3U) || (parsed_l86_data->mo == 5U) || (parsed_l86_data->mo == 7U) || \
    (parsed_l86_data->mo == 8U) || (parsed_l86_data->mo == 10U) || (parsed_l86_data->mo == 12U))
    {
        if(parsed_l86_data->dd < 31U)
        {
            parsed_l86_data->dd++;
        }
        else
        {
            if(parsed_l86_data->mo != 12U)
            {
                parsed_l86_data->mo++;
                parsed_l86_data->dd = 1U;
            }
            else
            {
                parsed_l86_data->mo = 1U;
                parsed_l86_data->dd = 1U;
                parsed_l86_data->yy++;
            }
        }
    }
    else if((parsed_l86_data->mo == 4U) || (parsed_l86_data->mo == 6U) || (parsed_l86_data->mo == 9U) || (parsed_l86_data->mo == 11U))
    {
        if(parsed_l86_data->dd < 30U)
        {
            parsed_l86_data->dd++;
        }
        else
        {
            parsed_l86_data->mo++;
            parsed_l86_data->dd = 1U;
        }
    }
    else if(parsed_l86_data->mo == 2U)
    {
        if(parsed_l86_data->dd < 28U)
        {
            parsed_l86_data->dd++;
        }
        else if(parsed_l86_data->dd == 28U)
        {
            if(l86_leap_year_check(parsed_l86_data->yy))
            {
                parsed_l86_data->dd++;
            }
            else
            {
                parsed_l86_data->mo++;
                parsed_l86_data->dd = 1U;
            }
        }
        else if(parsed_l86_data->dd == 29U)
        {
            parsed_l86_data->mo++;
            parsed_l86_data->dd = 1U;
        }
    }
}
