#include "w25q64jvssiq.h"
#include "string.h"

extern SPI_HandleTypeDef hspi1;

void w25q64_init(void)
{
	// read dev id
	uint8_t txData[5] = { RELEASEPOWD_DEVID, 0xFF, 0xFF, 0xFF, 0x00 };
	uint8_t rxData[10] = { 0 };

	HAL_GPIO_WritePin(SS1B_Flash_GPIO_Port, SS1B_Flash_Pin, GPIO_PIN_RESET);

	HAL_SPI_Transmit(&hspi1, &txData[0], 4, 4);
	HAL_SPI_Receive(&hspi1, &rxData[0], 1, 4); // 0x16

	HAL_GPIO_WritePin(SS1B_Flash_GPIO_Port, SS1B_Flash_Pin, GPIO_PIN_SET);

	// read manufaturer id and dev id

	txData[0] = READMANUFACTURER_DEVID;
	memset((void*) &txData[1], 0x00, 4);
	memset((void*) &rxData[0], 0x00, 10);

	HAL_GPIO_WritePin(SS1B_Flash_GPIO_Port, SS1B_Flash_Pin, GPIO_PIN_RESET);

	HAL_SPI_Transmit(&hspi1, &txData[0], 4, 4);
	HAL_SPI_Receive(&hspi1, &rxData[0], 2, 4); // 0xEF 0x16

	HAL_GPIO_WritePin(SS1B_Flash_GPIO_Port, SS1B_Flash_Pin, GPIO_PIN_SET);

	// read unique id

	txData[0] = UNIQUE_ID;
	memset((void*) &txData[1], 0x00, 4);
	memset((void*) &rxData[0], 0x00, 10);

	HAL_GPIO_WritePin(SS1B_Flash_GPIO_Port, SS1B_Flash_Pin, GPIO_PIN_RESET);

	HAL_SPI_Transmit(&hspi1, &txData[0], 5, 5);
	HAL_SPI_Receive(&hspi1, &rxData[0], 8, 8); // 8 byte unique id

	HAL_GPIO_WritePin(SS1B_Flash_GPIO_Port, SS1B_Flash_Pin, GPIO_PIN_SET);

	// read jedec id

	txData[0] = JEDEC_ID;
	memset((void*) &txData[1], 0x00, 4);
	memset((void*) &rxData[0], 0x00, 10);

	HAL_GPIO_WritePin(SS1B_Flash_GPIO_Port, SS1B_Flash_Pin, GPIO_PIN_RESET);

	HAL_SPI_Transmit(&hspi1, &txData[0], 1, 4);
	HAL_SPI_Receive(&hspi1, &rxData[0], 3, 4); // 0xEF memory type id	capacity id

	HAL_GPIO_WritePin(SS1B_Flash_GPIO_Port, SS1B_Flash_Pin, GPIO_PIN_SET);

	HAL_Delay(1000);
}


