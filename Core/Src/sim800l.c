#include "sim800l.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#include "ctype.h"

#define gsm_int_rx_buff_size 384
#define gsm_rx_buff_size 384

extern UART_HandleTypeDef huart1;
//extern UART_HandleTypeDef hlpuart1;

typedef struct
{
	uint8_t resp;
} sim800l_cgdcont_data_t;
sim800l_cgdcont_data_t sim800l_cgdcont_data =
{
	.resp = 0,
};
typedef struct
{
	uint8_t csqRssi[3];
	uint8_t csqBer[3];
	uint8_t resp;
} sim800l_csq_data_t;
sim800l_csq_data_t sim800l_csq_data =
{
	.csqRssi = {0, 0, 0},
	.csqBer = {0, 0, 0},
	.resp = 0,
};
typedef struct
{
	uint8_t netReg_stat;
	uint8_t resp;
} sim800l_creg_data_t;
sim800l_creg_data_t sim800l_creg_data =
{
	.netReg_stat = 0,
	.resp = 0,
};
typedef struct
{
	uint8_t netGReg_stat;
	uint8_t resp;
} sim800l_cgreg_data_t;
sim800l_cgreg_data_t sim800l_cgreg_data =
{
	.netGReg_stat = 0,
	.resp = 0,
};
typedef struct
{
	uint8_t resp;
} sim800l_ciphead_data_t;
sim800l_ciphead_data_t sim800l_ciphead_data =
{
	.resp = 0,
};
typedef struct
{
	uint8_t resp;
} sim800l_cipshowtp_data_t;
sim800l_cipshowtp_data_t sim800l_cipshowtp_data =
{
	.resp = 0,
};
typedef struct
{
	uint8_t resp;
} sim800l_cipqsend_data_t;
sim800l_cipqsend_data_t sim800l_cipqsend_data =
{
	.resp = 0,
};
typedef struct
{
	uint8_t state;
	uint8_t resp;
} sim800l_cipmux_data_t;
sim800l_cipmux_data_t sim800l_cipmux_data =
{
	.state = 0,
	.resp = 0,
};
typedef struct
{
	uint8_t state;
	uint8_t resp;
} sim800l_ciptka_data_t;
sim800l_ciptka_data_t sim800l_ciptka_data =
{
	.state = 0,
	.resp = 0,
};
typedef struct
{
	uint8_t resp;
} sim800l_cgatt_data_t;
sim800l_cgatt_data_t sim800l_cgatt_data =
{
	.resp = 0,
};
typedef struct
{
	uint8_t stat;
	uint8_t resp;
} sim800l_sapbr_data_t;
sim800l_sapbr_data_t sim800l_sapbr_data =
{
	.stat = 0,
	.resp = 0,
};
typedef struct
{
	uint8_t resp;
} sim800l_cntpcid_data_t;
sim800l_cntpcid_data_t sim800l_cntpcid_data =
{
	.resp = 0,
};
typedef struct
{
	uint8_t stat;
	uint8_t try_count;
	uint8_t resp;
} sim800l_cntp_data_t;
sim800l_cntp_data_t sim800l_cntp_data =
{
	.stat = 0,
	.try_count = 0,
	.resp = 0,
};
typedef struct
{
	uint8_t gprs_states[17];
} sim800l_cipstatus_data_t;
sim800l_cipstatus_data_t sim800l_cipstatus_data =
{
	.gprs_states = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
};
typedef struct
{
	uint8_t apn[10];
	uint8_t resp;
} sim800l_cstt_data_t;
sim800l_cstt_data_t sim800l_cstt_data =
{
	.apn = {0,0,0,0,0,0,0,0,0,0},
	.resp = 0,
};
typedef struct
{
	uint8_t resp;
} sim800l_ciicr_data_t;
sim800l_ciicr_data_t sim800l_ciicr_data =
{
	.resp = 0,
};
typedef struct
{
	uint8_t local_addr[16];
	uint8_t resp;
} sim800l_cifsr_data_t;
sim800l_cifsr_data_t sim800l_cifsr_data =
{
	.local_addr = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
	.resp = 0,
};
typedef struct
{
	uint8_t resp;
} sim800l_cipscont_data_t;
sim800l_cipscont_data_t sim800l_cipscont_data =
{
	.resp = 0,
};
typedef struct
{
	uint8_t resp;
} sim800l_atw_data_t;
sim800l_atw_data_t sim800l_atw_data =
{
	.resp = 0,
};
typedef struct
{
	uint8_t strlen;
	uint8_t tcpStat;
	uint8_t resp;
} sim800l_cipstart_data_t;
sim800l_cipstart_data_t sim800l_cipstart_data =
{
	.strlen = 0,
	.tcpStat = 4,
	.resp = 0,
};
sim800l_cclk_data_t sim800l_cclk_data =
{ 0 };
typedef struct
{
	uint32_t reply_stat;
	uint32_t reply_stat2;
} sim800l_global_stat_t;
sim800l_global_stat_t sim800l_global_stat =
{
	.reply_stat = 0,
	.reply_stat2 = 0,
};
typedef struct
{
	HAL_StatusTypeDef rxInterrupt_setStat;
	uint8_t charCount;
	uint16_t inChar;
	uint32_t rxlastTime;
} sim800l_txrx_t;
sim800l_txrx_t sim800l_txrx =
{ 0 };
uint16_t sim800l_error_stat = 0;
uint8_t gsm_rx_buffer[gsm_rx_buff_size] = {0};
volatile uint8_t gsm_int_rx_buff[gsm_int_rx_buff_size] = {0};
uint8_t gsm_rx_data = 0;
volatile uint8_t gsm_rx_buff_head = 0;
volatile uint8_t gsm_rx_buff_tail = 0;

uint8_t sim800l_ipd_handler(uint8_t *ptr);
uint8_t sim800l_cipsend_data(const char* message, uint8_t message_len);
int16_t sim800l_read(void);
_Bool sim800l_atOK_read(void);
_Bool sim800l_atERROR_read(void);
_Bool sim800l_cregData_read(void);
_Bool sim800l_cgregData_read(void);
_Bool sim800l_cipshut_read(void);
_Bool sim800l_cstt_handler(uint8_t* ptr);
_Bool sim800l_cregData_read(void);
_Bool sim800l_cgregData_read(void);
_Bool sim800l_ciptka_read(void);
_Bool sim800l_cipmux_read(void);
_Bool sim800l_atCommand_check(void);
_Bool sim800l_commandEcho_on(void);
_Bool sim800l_pdpContext_set(void);
_Bool sim800l_csq_query(void);
_Bool sim800l_creg_check(void);
_Bool sim800l_cgreg_check(void);
_Bool sim800l_ciphead_set(void);
_Bool sim800l_cipshowtp_set(void);
_Bool sim800l_cipqsend_set(void);
_Bool sim800l_cipmux_ask(void);
_Bool sim800l_cipmux_set(void);
_Bool sim800l_ciptka_ask(void);
_Bool sim800l_ciptka_set(void);
_Bool sim800l_cgatt_set(void);
_Bool sim800l_sapbr_query(void);
_Bool sim800l_sapbr_set(void);
_Bool sim800l_ntp_set(void);
_Bool sim800l_cclk_ask(void);
_Bool sim800l_cstt_set(void);
_Bool sim800l_ciicr_set(void);
_Bool sim800l_cifsr_set(void);
_Bool sim800l_cipscont_set(void);
_Bool sim800l_atw_set(void);
_Bool sim800l_cipstart_set(void);
_Bool sim800l_send(const char* at_command, uint32_t at_reply, uint8_t at_timeout, uint8_t at_command_len, uint8_t flag_numb);
void sim800l_atOK_clear_flag(void);
void sim800l_atERROR_clear_flag(void);
void sim800l_cipshut_clear_flag(void);
void sim800l_startup(void);
void sim800l_force_restart(void);
void sim800l_debug_print(const char* message);
void sim800l_atResp_read(uint8_t* ptr, uint8_t* atResp, uint8_t* firstPtr, const char* charSequence);
void sim800l_reply_list(uint16_t element_count);
void sim800l_cgdcont_handler(uint8_t* ptr);
void sim800l_creg_handler(uint8_t* ptr);
void sim800l_cgreg_handler(uint8_t* ptr);
void sim800l_ciphead_handler(uint8_t* ptr);
void sim800l_cipshowtp_handler(uint8_t* ptr);
void sim800l_cipqsend_handler(uint8_t* ptr);
void sim800l_cipmux_handler(uint8_t* ptr);
void sim800l_ciptka_handler(uint8_t* ptr);
void sim800l_cgatt_handler(uint8_t* ptr);
void sim800l_sapbr_handler(uint8_t* ptr);
void sim800l_cntpcid_handler(uint8_t* ptr);
void sim800l_cntp_handler(uint8_t* ptr);
void sim800l_cclk_handler(uint8_t* ptr);
void sim800l_csq_handler(uint8_t* ptr);
void sim800l_cipstatus_handler(uint8_t* ptr);
void sim800l_ciicr_handler(uint8_t* ptr);
void sim800l_cifsr_handler(uint8_t* ptr);
void sim800l_cipscont_handler(uint8_t* ptr);
void sim800l_atw_handler(uint8_t* ptr);
void sim800l_cipstart_handler(uint8_t* ptr);
void sim800l_tcpfailed_handler(uint8_t* ptr);
void sim800l_ntpstat_handler(uint8_t* ptr);
void sim800l_echoOn(void);
void sim800l_pdpSetvoid(void);
void sim800l_cregCheck(void);
void sim800l_cgregCheck(void);
void sim800l_cipheadSet(void);
void sim800l_cipshowtpSet(void);
void sim800l_cipqsendSet(void);
void sim800l_cipmuxSet(void);
void sim800l_ciptkaSet(void);
void sim800l_cgattSet(void);
void sim800l_csttSet(void);
void sim800l_ciicrSet(void);
void sim800l_cifsrSet(void);
void sim800l_saveTCP_profile(void);
void sim800l_save_profile(void);
void sim800l_tcpStart(void);
void sim800l_reconnect(uint8_t state);

void sim800l_start_rx(void)
{
	sim800l_txrx.rxInterrupt_setStat = HAL_UART_Receive_IT(&huart1, &gsm_rx_data, 1);
}
void sim800l_startup(void)
{
	sim800l_debug_print("SIM800l startup ");
	for(uint8_t count = 0; count < 8; count++)
	{
		sim800l_debug_print(".");
		HAL_Delay(1000);
	}
	sim800l_debug_print("\r\n\0");
}
void sim800l_rx_handler(void)
{
	uint8_t rx_index = (gsm_rx_buff_head + 1) % gsm_int_rx_buff_size;
	if(rx_index != gsm_rx_buff_tail)
	{
		gsm_int_rx_buff[gsm_rx_buff_head] = gsm_rx_data;
		gsm_rx_buff_head = rx_index;
	}
}
uint8_t sim800l_data_available(void)
{
	uint8_t available = ((gsm_int_rx_buff_size + gsm_rx_buff_head - gsm_rx_buff_tail) % gsm_int_rx_buff_size);
	return available;
}
int16_t sim800l_read(void)
{
	if(gsm_rx_buff_head == gsm_rx_buff_tail)
	{
		return -1;
	}
	else
	{
		uint8_t c = gsm_int_rx_buff[gsm_rx_buff_tail];
		gsm_rx_buff_tail = (gsm_rx_buff_tail + 1) % gsm_int_rx_buff_size;
		return (uint8_t) c;
	}
}
void sim800l_to_2ndbuff(void)
{
	sim800l_txrx.inChar = sim800l_read();
	if(sim800l_txrx.inChar != -1)
	{
		gsm_rx_buffer[sim800l_txrx.charCount] = sim800l_txrx.inChar;
		sim800l_txrx.charCount++;
		if(sim800l_txrx.charCount == gsm_rx_buff_size)
		{
			sim800l_txrx.charCount = 0;
		}
	}
	sim800l_txrx.rxlastTime = HAL_GetTick();
}
void sim800l_2ndbuff_process(void)
{
	uint16_t data_index = 64;
	if(HAL_GetTick() - sim800l_txrx.rxlastTime > 30)
	{
		return;
	}
	for(uint16_t element_index = 0; element_index < gsm_rx_buff_size; element_index++)
	{
		if(gsm_rx_buffer[element_index] != '\0')
		{
			data_index = element_index;
			break;
		}
	}
	if(((HAL_GetTick() - sim800l_txrx.rxlastTime) > 10) && data_index != 64)
	{
		sim800l_reply_list(data_index);
		memset(&gsm_rx_buffer[0], 0, gsm_rx_buff_size);
		sim800l_txrx.charCount = 0;
	}
}
void sim800l_force_restart(void)
{
  HAL_GPIO_WritePin(MDMRST_GPIO_Port, MDMRST_Pin, GPIO_PIN_RESET);
  HAL_Delay(1000);
  HAL_GPIO_WritePin(swMDM_GPIO_Port, swMDM_Pin, GPIO_PIN_SET);
  HAL_Delay(500);
  HAL_GPIO_WritePin(MDMRST_GPIO_Port, MDMRST_Pin, GPIO_PIN_SET);
  HAL_Delay(500);
  HAL_GPIO_WritePin(swMDM_GPIO_Port, swMDM_Pin, GPIO_PIN_RESET);
  HAL_Delay(15000);
}
void sim800l_debug_print(const char* message)
{
	//HAL_UART_Transmit(&hlpuart1, (uint8_t*) &message[0], strlen(message), 50);
}
_Bool sim800l_send(const char* at_command, uint32_t at_reply, uint8_t at_timeout, uint8_t at_command_len, uint8_t flag_numb)
{
	__HAL_UART_CLEAR_FLAG(&huart1, UART_CLEAR_OREF);
	__HAL_UART_FLUSH_DRREGISTER(&huart1);
	if(at_command_len == 0)
	{
		HAL_UART_Transmit(&huart1, (uint8_t*) &at_command[0], strlen(at_command), strlen(at_command));
	}
	else
	{
		HAL_UART_Transmit(&huart1, (uint8_t*) &at_command[0], at_command_len, at_command_len);
	}
	if(sim800l_txrx.rxInterrupt_setStat != HAL_OK)
	{
		sim800l_start_rx();
	}
	uint32_t timeStamp = HAL_GetTick();
	while((HAL_GetTick() - timeStamp) < at_timeout)
	{
		if(flag_numb == 1)
		{
			if(sim800l_global_stat.reply_stat & at_reply)
			{
				sim800l_global_stat.reply_stat &= ~(at_reply);
				return 1;
			}
		}
		else if(flag_numb == 2)
		{
			if(sim800l_global_stat.reply_stat2 & at_reply)
			{
				sim800l_global_stat.reply_stat2 &= ~(at_reply);
				return 1;
			}
		}
	}
	return 0;
}

_Bool sim800l_ntpSync_read(void)
{
	return sim800l_cclk_data.sync;
}
void sim800l_cipshut_clear_flag(void)
{
	sim800l_global_stat.reply_stat &= ~(0x20000000);
}
_Bool sim800l_cipshut_read(void)
{
	if(sim800l_global_stat.reply_stat & 0x20000000)
	{
		return 1;
	}
	return 0;
}
void sim800l_atOK_clear_flag(void)
{
	sim800l_global_stat.reply_stat &= ~(0x40000000);
}
_Bool sim800l_atOK_read(void)
{
	if(sim800l_global_stat.reply_stat & 0x40000000)
	{
		return 1;
	}
	return 0;
}
void sim800l_atERROR_clear_flag(void)
{
	sim800l_global_stat.reply_stat &= ~(0x80000000);
}
_Bool sim800l_atERROR_read(void)
{
	if(sim800l_global_stat.reply_stat & 0x80000000)
	{
		return 1;
	}
	return 0;
}
_Bool sim800l_cregData_read(void)
{
	if(sim800l_creg_data.netReg_stat == 1)
	{
		return 1;
	}
	return 0;
}
_Bool sim800l_cgregData_read(void)
{
	if(sim800l_cgreg_data.netGReg_stat == 1)
	{
		return 1;
	}
	return 0;
}
_Bool sim800l_ciptka_read(void)
{
	if(sim800l_ciptka_data.state > 0)
	{
		return 1;
	}
	return 0;
}
_Bool sim800l_cipmux_read(void)
{
	if(sim800l_cipmux_data.state == 1)
	{
		return 1;
	}
	return 0;
}
void sim800l_tcpConnect_clear_flag(void)
{
	sim800l_global_stat.reply_stat &= ~(0x10000000);
}
_Bool sim800l_tcpConnect_read(void)
{
	if(sim800l_global_stat.reply_stat & 0x10000000)
	{
		return 1;
	}
	return 0;
}
void sim800l_tcpAConnect_clear_flag(void)
{
	sim800l_global_stat.reply_stat &= ~(0x10000000);
}
_Bool sim800l_tcpAConnect_read(void)
{
	if(sim800l_global_stat.reply_stat & 0x10000000)
	{
		return 1;
	}
	return 0;
}
void sim800l_tcpFailed_clear_flag(void)
{
	sim800l_global_stat.reply_stat &= ~(0x8000000);
}
_Bool sim800l_tcpFailed_read(void)
{
	if(sim800l_global_stat.reply_stat & 0x8000000)
	{
		return 1;
	}
	return 0;
}
void sim800l_sendOK_clear_flag(void)
{
	sim800l_global_stat.reply_stat &= ~(0x1000000);
}
_Bool sim800l_sendOK_read(void)
{
	if(sim800l_global_stat.reply_stat & 0x1000000)
	{
		return 1;
	}
	return 0;
}
void sim800l_sendFAIL_clear_flag(void)
{
	sim800l_global_stat.reply_stat &= ~(0x800000);
}
_Bool sim800l_sendFAIL_read(void)
{
	if(sim800l_global_stat.reply_stat & 0x800000)
	{
		return 1;
	}
	return 0;
}
uint8_t sim800l_tcpstat_read(void)
{
	return sim800l_cipstart_data.tcpStat;
}
uint16_t sim800l_errorcode_read(void)
{
	return sim800l_error_stat;
}

void sim800l_atResp_read(uint8_t* ptr, uint8_t* atResp, uint8_t* firstPtr, const char* charSequence)
{
	uint8_t dataLen = 0;
	uint8_t internalBuff[6] = {0};
	uint8_t* ptr1 = ptr;
	ptr = ((uint8_t*) strstr((const char*) ptr, charSequence));
	if(ptr != NULL)
	{
		ptr += strlen(charSequence);
		dataLen = ((uint8_t*) strchr((const char*) ptr, '\n') - ptr);
		if(dataLen < 7)
		{
			memcpy((void*) &internalBuff[0], ptr, --dataLen);
		}
		else
		{
			memcpy((void*) &internalBuff[0], ptr, 5);
		}
		if((uint8_t*) strstr((const char*) &internalBuff[0], "OK") != NULL)
		{
			*atResp = 1;
		}
	}
	if(((ptr + dataLen) - firstPtr) > 0)
	{
		memset((void*) firstPtr, 0xff, ((ptr + dataLen) - firstPtr) + 2);
	}
	else
	{
		memset((void*) firstPtr, 0xff, (ptr1 - firstPtr) + 1);
	}
}
void sim800l_cgdcont_handler(uint8_t* ptr)
{
	sim800l_cgdcont_data.resp = 0;
	uint8_t* firstPtr = ptr;
	sim800l_atResp_read((ptr + 10), &sim800l_cgdcont_data.resp, firstPtr, "\r\r\n");
}
void sim800l_csq_handler(uint8_t* ptr)
{
	uint8_t csqBuff[6] = {0};
	uint8_t* firstPtr = ptr;
	ptr += 15;
	memset((void*) &sim800l_csq_data.csqRssi[0], 0, 3);
	memset((void*) &sim800l_csq_data.csqBer[0], 0, 3);
	memcpy((void*) &csqBuff[0], ptr, 5);
	sscanf((const char*) &csqBuff[0], "%2[^,],%2[^\r]", (char*) &sim800l_csq_data.csqRssi, (char*) &sim800l_csq_data.csqBer);
	sim800l_atResp_read(ptr, &sim800l_csq_data.resp, firstPtr, "\r\n\r\n");
}
void sim800l_creg_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	sscanf((const char*) ptr, "AT+CREG?\r\r\n+CREG: %*[^,],%1hhu\r\n\r\nOK\r\n", &sim800l_creg_data.netReg_stat);
	sim800l_atResp_read((ptr + 21), &sim800l_creg_data.resp, firstPtr, "\r\n\r\n");
}
void sim800l_cgreg_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	sscanf((const char*) ptr, "AT+CGREG?\r\r\n+CGREG: %*[^,],%1hhu\r\n\r\nOK\r\n", &sim800l_cgreg_data.netGReg_stat);
	sim800l_atResp_read((ptr + 23), &sim800l_cgreg_data.resp, firstPtr, "\r\n\r\n");
}
void sim800l_ciphead_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	sim800l_atResp_read((ptr + 12), &sim800l_ciphead_data.resp, firstPtr, "\r\r\n");
}
void sim800l_cipshowtp_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	sim800l_atResp_read((ptr + 14), &sim800l_cipshowtp_data.resp, firstPtr, "\r\r\n");
}
void sim800l_cipqsend_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	sim800l_atResp_read((ptr + 13), &sim800l_cipqsend_data.resp, firstPtr, "\r\r\n");
}
void sim800l_cipmux_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	ptr += 9;
	if(*ptr == '=')
	{
		sim800l_atResp_read((ptr + 2), &sim800l_cipmux_data.resp, firstPtr, "\r\r\n");
	}
	else if(*ptr == '?')
	{
		sscanf((const char*) firstPtr, "AT+CIPMUX?\r\r\n+CIPMUX: %1hhu\r\n\r\nOK\r\n", &sim800l_cipmux_data.state);
		sim800l_cipmux_data.resp = 1;
		memset((void*) firstPtr, 0xff, 31);
	}
}
void sim800l_ciptka_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	ptr += 9;
	if(*ptr == '=')
	{
		sim800l_atResp_read(ptr, &sim800l_ciptka_data.resp, firstPtr, "\r\r\n");
		sim800l_ciptka_data.state = 0;
	}
	else if(*ptr == '?')
	{
		uint8_t temp[40] = {0};
		memcpy((void*) &temp[0], (void*) firstPtr, 39);
		sim800l_ciptka_data.state = strcmp((const char*) &temp[0], "AT+CIPTKA?\r\r\n+CIPTKA: 1,30,30,9\r\n\r\nOK\r\n");
		sim800l_atResp_read(ptr, &sim800l_ciptka_data.resp, firstPtr, "\r\n\r\n");
	}
}
void sim800l_cgatt_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	sim800l_atResp_read((ptr + 10), &sim800l_cgatt_data.resp, firstPtr, "\r\r\n");
}
void sim800l_cipstatus_handler(uint8_t* ptr)
{
	memset((void*) &sim800l_cipstatus_data.gprs_states[0], 0, 17);
	uint8_t* firstPtr = ptr;
	sscanf((const char*) ptr, "AT+CIPSTATUS\r\r\nOK\r\n\r\nSTATE: %16[^\r]\r\n", (char*) &sim800l_cipstatus_data.gprs_states);
	memset((void*) firstPtr, 0xff, (strlen((const char*) &sim800l_cipstatus_data.gprs_states[0]) + 30));
}
_Bool sim800l_cstt_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	sim800l_cstt_data.resp = 0;
	if(*(ptr + 7) == '?')
	{
		if(sscanf((const char*) ptr, "AT+CSTT?\r\r\n+CSTT: \"%10[^\"]\",\"\",\"\"", (char*) &sim800l_cstt_data.apn[0]) == 1)
		{
			sim800l_atResp_read(ptr, &sim800l_cstt_data.resp, firstPtr, "\r\n\r\n");
			sim800l_global_stat.reply_stat |= 0x20;
			if(sim800l_cstt_data.resp)
			{
				return 1;
			}
		}
	}
	else if(*(ptr + 7) == '=')
	{
		if(sscanf((const char*) ptr, "AT+CSTT=\"%10[^\"]\",\"\",\"\"", (char*) &sim800l_cstt_data.apn[0]) == 1)
		{
			sim800l_atResp_read(ptr, &sim800l_cstt_data.resp, firstPtr, "\r\r\n");
			sim800l_global_stat.reply_stat |= 0x40;
			if(sim800l_cstt_data.resp)
			{
				return 1;
			}
		}
	}
	else if(*(ptr + 7) == '\r')
	{
		sim800l_atResp_read(ptr, &sim800l_cstt_data.resp, firstPtr, "\r\r\n");
		ptr = ((uint8_t*) strstr((const char*) ptr, "\r\r\n"));
		sim800l_global_stat.reply_stat |= 0x80;
		if(sim800l_cstt_data.resp)
		{
			return 1;
		}
	}
	return 0;
}
void sim800l_ciicr_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	sim800l_atResp_read((ptr + 8), &sim800l_ciicr_data.resp, firstPtr, "\r\r\n");
}
void sim800l_cifsr_handler(uint8_t* ptr)
{
	uint8_t status = 0;
	uint8_t* firstPtr = ptr;
	status = sscanf((const char*) ptr, "AT+CIFSR\r\r\n%15[^\r]\r\n", (char*) &sim800l_cifsr_data.local_addr[0]);
	memset((void*) firstPtr, 0xff, (13 + strlen((const char*) &sim800l_cifsr_data.local_addr[0])));
	if(status == 1)
	{
		sim800l_cifsr_data.resp = 1;
	}
	else
	{
		sim800l_cifsr_data.resp = 0;
	}
}
void sim800l_cipscont_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	sim800l_atResp_read((ptr + 11), &sim800l_cipscont_data.resp, firstPtr, "\r\r\n");
}
void sim800l_atw_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	sim800l_atResp_read((ptr + 5), &sim800l_atw_data.resp, firstPtr, "\r\r\n");
}
void sim800l_cipstart_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	sim800l_atResp_read((ptr + 9), &sim800l_cipstart_data.resp, firstPtr, "\r\r\n");
}
void sim800l_tcpfailed_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	ptr = ((uint8_t*) strstr((const char*) ptr, "\r\n\r\n")) + 17;
	memset((void*) firstPtr, 0xff,(ptr - firstPtr + 1));
}
void sim800l_ntpstat_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	ptr += 9;
	if(*ptr == '1')
	{
		sim800l_cntp_data.stat = 1;
		memset((void*) firstPtr, 0xff, 12);
	}
	else
	{
		sim800l_cntp_data.stat = 2;
		memset((void*) firstPtr, 0xff, 13);
	}
}
void sim800l_sapbr_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	if(*(ptr + 9) == '2')
	{
		uint8_t internal_buff[53] = {0};
		uint8_t strlen = 0;
		ptr = ((uint8_t*) strstr((const char*) ptr, "OK\r\n") + 3);
		strlen = (ptr - firstPtr) + 1;
		if(strlen > 52)
		{
			memcpy((void*) &internal_buff[0], (void*) firstPtr, 52);
		}
		else
		{
			memcpy((void*) &internal_buff[0], (void*) firstPtr, strlen);
		}
		sscanf((const char*) &internal_buff[15], "+SAPBR: 1,%1hhu,%*[\n]", &sim800l_sapbr_data.stat);
		sim800l_atResp_read((ptr - 8), &sim800l_sapbr_data.resp, firstPtr, "\r\n\r\n");
	}
	else
	{
		sim800l_atResp_read((ptr + 12), &sim800l_sapbr_data.resp, firstPtr, "\r\r\n");
	}
}
void sim800l_cntpcid_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	sim800l_atResp_read((ptr), &sim800l_cntpcid_data.resp, firstPtr, "\r\r\n");
}
void sim800l_cntp_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	sim800l_atResp_read((ptr + 3), &sim800l_cntp_data.resp, firstPtr, "\r\r\n");
}
void sim800l_cclk_handler(uint8_t* ptr)
{
	uint8_t* firstPtr = ptr;
	uint8_t status = 0;
	status = sscanf((const char*) ptr, "AT+CCLK?\r\r\n+CCLK: \"%2hhu/%2hhu/%2hhu,%2hhu:%2hhu:%2hhu+%2hhu\"", &sim800l_cclk_data.year, &sim800l_cclk_data.month, &sim800l_cclk_data.date, &sim800l_cclk_data.hour, &sim800l_cclk_data.minute, &sim800l_cclk_data.second, &sim800l_cclk_data.timezone);
	if(status == 7)
	{
		sim800l_cclk_data.sync = 1;
	}
	else
	{
		sim800l_cclk_data.sync = 0;
	}
	sim800l_atResp_read((ptr), &sim800l_cclk_data.resp, firstPtr, "\r\n\r\n");
}
void sim800l_pdpdeact_react(void)
{
	sim800l_reconnect(1);
}
void sim800l_tcp_reconnect(void)
{
	sim800l_reconnect(0);
}
_Bool sim800l_atCommand_check(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("AT\r\n\0", 0x02, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		sim800l_atOK_clear_flag();
		HAL_Delay(1000);
	}
	HAL_Delay(1000);
	if(at_try >= 3)
	{
		return 0;
	}
	return 1;
}
_Bool sim800l_commandEcho_on(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("ATE1\r\n\0", 0x04, 200, 0, 1) && at_try < 3)
	{
		at_try++;
		sim800l_atOK_clear_flag();
		HAL_Delay(1000);
	}
	HAL_Delay(1000);
	if(at_try >= 3)
	{
		return 0;
	}
	return 1;
}
_Bool sim800l_pdpContext_set(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("AT+CGDCONT=1,\"IP\",\"internet\",\"0.0.0.0\",0,0\r\n\0", 0x08, 200, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	HAL_Delay(1000);
	if(sim800l_cgdcont_data.resp)
	{
		sim800l_cgdcont_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_csq_query(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("AT+CSQ\r\n\0", 0x10, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	HAL_Delay(1000);
	if(sim800l_csq_data.resp)
	{
		sim800l_csq_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_creg_check(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("AT+CREG?\r\n\0", 0x1000, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	if(sim800l_creg_data.resp)
	{
		sim800l_creg_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_cgreg_check(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("AT+CGREG?\r\n\0", 0x8000, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	if(sim800l_cgreg_data.resp)
	{
		sim800l_cgreg_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_ciphead_set(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("AT+CIPHEAD=1\r\n\0", 0x4000, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	if(sim800l_ciphead_data.resp)
	{
		sim800l_ciphead_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_cipshowtp_set(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("AT+CIPSHOWTP=1\r\n\0", 0x10000, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	if(sim800l_cipshowtp_data.resp)
	{
		sim800l_cipshowtp_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_cipqsend_set(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("AT+CIPQSEND=0\r\n\0", 0x30000, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	if(sim800l_cipqsend_data.resp)
	{
		sim800l_cipqsend_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_cipmux_ask(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("AT+CIPMUX?\r\n\0", 0x40000, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	if(sim800l_cipmux_data.resp)
	{
		sim800l_cipmux_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_cipmux_set(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("AT+CIPMUX=0\r\n\0", 0x40000, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	if(sim800l_cipmux_data.resp)
	{
		sim800l_cipmux_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_ciptka_ask(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("AT+CIPTKA?\r\n\0", 0x20000, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	if(sim800l_ciptka_data.resp)
	{
		sim800l_ciptka_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_ciptka_set(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("AT+CIPTKA=1,30,30,9\r\n\0", 0x20000, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	if(sim800l_ciptka_data.resp)
	{
		sim800l_ciptka_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_cgatt_set(void)
{
	uint8_t at_try = 0;
	uint32_t time_stamp = 0;
	while(!sim800l_send("AT+CGATT=1\r\n\0", 0x400, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	if(!sim800l_cgatt_data.resp)
	{
		HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
		time_stamp = HAL_GetTick();
		while((HAL_GetTick() - time_stamp) < 15000)
		{
			if(sim800l_atOK_read())
			{
				sim800l_atOK_clear_flag();
				sim800l_cgatt_data.resp = 1;
				break;
			}
			else if(sim800l_atERROR_read())
			{
				sim800l_atERROR_clear_flag();
				sim800l_cgatt_data.resp = 0;
				break;
			}
		}
		HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
	}
	if(sim800l_cgatt_data.resp)
	{
		sim800l_cgatt_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_cipstatus_query(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("AT+CIPSTATUS\r\n\0", 0x100, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	HAL_Delay(100);
	if(at_try >= 3)
	{
		return 0;
	}
	return 1;
}
_Bool sim800l_cipshut_set(void)
{
	uint8_t at_try = 0;
	uint8_t status = 0;
	uint32_t time_stamp = 0;
	while(!sim800l_send("AT+CIPSHUT\r\n\0", 0x200, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	if(at_try < 3)
	{
		HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
		time_stamp = HAL_GetTick();
		while((HAL_GetTick() - time_stamp) < 70000)
		{
			if(sim800l_cipshut_read())
			{
				sim800l_cipshut_clear_flag();
				status = 1;
				break;
			}
			else if(sim800l_atERROR_read())
			{
				sim800l_atERROR_clear_flag();
				break;
			}
		}
		HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
	}
	if(status)
	{
		return 1;
	}
	return 0;
}
_Bool sim800l_cstt_set(void)
{
	uint8_t at_try = 0;
	sim800l_cstt_data.resp = 0;
	while(at_try < 4)
	{
		at_try++;
		if(sim800l_cipstatus_query())
		{
			HAL_Delay(500);
			if(sim800l_send("AT+CSTT?\r\n\0", 0x20, 100, 0, 1))
			{
				HAL_Delay(500);
				if(strcmp("internet", (const char*) &sim800l_cstt_data.apn[0]) == 0)
				{
					if(strcmp("IP INITIAL", (const char*) &sim800l_cipstatus_data.gprs_states[0]) == 0)
					{
						HAL_Delay(500);
						sim800l_send("AT+CSTT\r\n\0", 0x80, 50, 0, 1);
					}
					else
					{
						if((strcmp("IP START", (const char*) &sim800l_cipstatus_data.gprs_states[0]) == 0) || (strcmp("CONNECT OK", (const char*) &sim800l_cipstatus_data.gprs_states[0]) == 0))
						{
							HAL_Delay(1000);
							break;
						}
						HAL_Delay(500);
						sim800l_cipshut_set();
					}
				}
				else
				{
					if(strcmp("IP INITIAL", (const char*) &sim800l_cipstatus_data.gprs_states[0]) == 0)
					{
						HAL_Delay(500);
						sim800l_send("AT+CSTT=\"internet\",\"\",\"\"\r\n\0", 0x40, 50, 0, 1);
					}
					else
					{
						HAL_Delay(500);
						sim800l_cipshut_set();
					}
				}
			}
		}
		HAL_Delay(1000);
	}
	sim800l_cstt_data.resp = 0;
	if(at_try >= 4)
	{
		return 0;
	}
	return 1;
}
_Bool sim800l_ciicr_set(void)
{
	uint8_t at_try = 0;
	uint32_t time_stamp = 0;
	while(at_try < 3)
	{
		at_try++;
		if(sim800l_cipstatus_query())
		{
			if(strcmp("IP START", (const char*) &sim800l_cipstatus_data.gprs_states[0]) == 0)
			{
				if(sim800l_send("AT+CIICR\r\n\0", 0x800, 100, 0, 1))
				{
					HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
					time_stamp = HAL_GetTick();
					while((HAL_GetTick() - time_stamp) < 90000)
					{
						if(sim800l_atOK_read() || sim800l_ciicr_data.resp)
						{
							sim800l_ciicr_data.resp = 1;
							sim800l_atOK_clear_flag();
							break;
						}
						else if(sim800l_atERROR_read())
						{
							sim800l_atERROR_clear_flag();
							break;
						}
					}
					HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
					break;
				}
			}
			else if(strcmp("CONNECT OK", (const char*) &sim800l_cipstatus_data.gprs_states[0]) == 0)
			{
				sim800l_ciicr_data.resp = 1;
				break;
			}
		}
		HAL_Delay(1000);
	}
	if(sim800l_ciicr_data.resp)
	{
		sim800l_ciicr_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_cifsr_set(void)
{
	uint8_t at_try = 0;
	while(at_try < 3)
	{
		at_try++;
		if(sim800l_cipstatus_query())
		{
			if(strcmp("IP GPRSACT", (const char*) &sim800l_cipstatus_data.gprs_states[0]) == 0)
			{
				if(sim800l_send("AT+CIFSR\r\n\0", 0x2000, 50, 0, 1))
				{
					if(sim800l_cifsr_data.resp)
					{
						break;
					}
					else if(sim800l_atERROR_read())
					{
						sim800l_atERROR_clear_flag();
						break;
					}
				}
			}
			else if(strcmp("CONNECT OK", (const char*) &sim800l_cipstatus_data.gprs_states[0]) == 0)
			{
				sim800l_cifsr_data.resp = 1;
				break;
			}
		}
		HAL_Delay(1000);
	}
	if(sim800l_cifsr_data.resp)
	{
		sim800l_cifsr_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_cipscont_set(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("AT+CIPSCONT\r\n\0", 0x80000, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	if(sim800l_cipscont_data.resp)
	{
		sim800l_cipscont_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_atw_set(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("AT&W0\r\n\0", 0x100000, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	if(sim800l_atw_data.resp)
	{
		sim800l_atw_data.resp = 0;
		return 1;
	}
	return 0;
}
uint8_t sim800l_cipsend_data(const char* message, uint8_t message_len)
{
	uint8_t at_try = 0;
	uint8_t status = 0;
	uint32_t time_stamp = 0;
	while(at_try < 3)
	{
		at_try++;
		if(sim800l_send("AT+CIPSEND\r\n\0", 0x400000, 100, 12, 1))
		{
			if(sim800l_send(message, 0x00, 200, message_len, 1) == 0)
			{
				break;
			}
		}
		HAL_Delay(2000);
	}
	if(at_try < 3)
	{
		time_stamp = HAL_GetTick();
		while(HAL_GetTick() - time_stamp < 15000)
		{
			if(sim800l_sendOK_read())
			{
				sim800l_sendOK_clear_flag();
				status = 1;
				break;
			}
			if(sim800l_sendFAIL_read())
			{
				sim800l_sendFAIL_clear_flag();
				status = 2;
				break;
			}
		}
		if(status == 0)
		{
			status = 3;
		}
		if(status == 1)
		{
			// Reserved for CONNACK handling if needed
		}
	}
	return status;
}
_Bool sim800l_cipstart_set(void)
{
	uint32_t time_stamp = 0;
	uint8_t at_try = 0;
	sim800l_cipstart_data.strlen = strlen((const char*) endpoint_connect);
	while(!sim800l_send((const char*) endpoint_connect, 0x200000, 100, 0, 1) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	if(sim800l_cipstart_data.resp)
	{
		HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
		time_stamp = HAL_GetTick();
		while((HAL_GetTick() - time_stamp) < 160000)
		{
			if(sim800l_tcpConnect_read())
			{
				sim800l_tcpConnect_clear_flag();
				sim800l_cipstart_data.resp = 1;
				sim800l_cipstart_data.tcpStat = 0;
				break;
			}
			else if(sim800l_tcpFailed_read())
			{
				sim800l_tcpFailed_clear_flag();
				sim800l_cipstart_data.resp = 0;
				sim800l_cipstart_data.tcpStat = 1;
				break;
			}
		}
		HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
	}
	else if(sim800l_tcpAConnect_read())
	{
		sim800l_tcpAConnect_clear_flag();
		sim800l_cipstart_data.resp = 1;
		sim800l_cipstart_data.tcpStat = 0;
	}
	if(sim800l_cipstart_data.resp)
	{
		sim800l_cipstart_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_sapbr_query(void)
{
	uint8_t at_try = 0;
	while(!sim800l_send("AT+SAPBR=2,1\r\n\0", 0x01, 100, 0, 2) && at_try < 3)
	{
		at_try++;
		HAL_Delay(1000);
	}
	HAL_Delay(100);
	if(at_try >= 3)
	{
		return 0;
	}
	return 1;
}
_Bool sim800l_sapbr_set(void)
{
	uint8_t at_try = 0;
	uint32_t time_stamp = 0;
	while(at_try < 3)
	{
		at_try++;
		if(sim800l_sapbr_query())
		{
			if(sim800l_sapbr_data.stat != 1)
			{
				sim800l_sapbr_data.stat = 0;
				if(sim800l_send("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"\r\n\0", 0x01, 100, 0, 2))
				{
					sim800l_sapbr_data.resp = 0;
					if(sim800l_send("AT+SAPBR=3,1,\"APN\",\"internet\"\r\n\0", 0x01, 100, 0, 2))
					{
						sim800l_sapbr_data.resp = 0;
						if(sim800l_send("AT+SAPBR=1,1\r\n\0", 0x01, 100, 0, 2))
						{
							if(!sim800l_sapbr_data.resp)
							{
								HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
								time_stamp = HAL_GetTick();
								while((HAL_GetTick() - time_stamp) < 120000)
								{
									if(sim800l_atOK_read())
									{
										sim800l_sapbr_data.resp = 1;
										sim800l_atOK_clear_flag();
										break;
									}
									else if(sim800l_atERROR_read())
									{
										sim800l_atERROR_clear_flag();
										break;
									}
								}
								HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
							}
							break;
						}
					}
				}
			}
			else
			{
				sim800l_sapbr_data.stat = 0;
				sim800l_sapbr_data.resp = 1;
				break;
			}
		}
		HAL_Delay(1000);
	}
	if(sim800l_sapbr_data.resp)
	{
		sim800l_sapbr_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_ntp_set(void)
{
	uint8_t at_try = 0;
	uint32_t time_stamp = 0;
	while(at_try < 3)
	{
		at_try++;
		if(sim800l_send("AT+CNTPCID=1\r\n\0", 0x02, 100, 0, 2))
		{
			sim800l_cntpcid_data.resp = 0;
			uint8_t ntp_server[35] =
			{ 0 };
			switch (sim800l_cntp_data.try_count)
			{
			case 0 :
				memcpy((void*) &ntp_server[0], "AT+CNTP=\"0.id.pool.ntp.org\",28\r\n\0", 33);
				break;
			case 1 :
				memcpy((void*) &ntp_server[0], "AT+CNTP=\"1.id.pool.ntp.org\",28\r\n\0", 33);
				break;
			case 2 :
				memcpy((void*) &ntp_server[0], "AT+CNTP=\"2.id.pool.ntp.org\",28\r\n\0", 33);
				break;
			case 3 :
				memcpy((void*) &ntp_server[0], "AT+CNTP=\"3.id.pool.ntp.org\",28\r\n\0", 33);
				break;
			case 4	:
				memcpy((void*) &ntp_server[0], "AT+CNTP=\"time.google.com\",28\r\n\0", 31);
				break;
			case 5	:
				memcpy((void*) &ntp_server[0], "AT+CNTP=\"time1.google.com\",28\r\n\0", 32);
				break;
			case 6	:
				memcpy((void*) &ntp_server[0], "AT+CNTP=\"time2.google.com\",28\r\n\0", 32);
				break;
			case 7	:
				memcpy((void*) &ntp_server[0], "AT+CNTP=\"time3.google.com\",28\r\n\0", 32);
				break;
			case 8	:
				memcpy((void*) &ntp_server[0], "AT+CNTP=\"time4.google.com\",28\r\n\0", 32);
				break;
			}
			if(sim800l_send((const char*) &ntp_server[0], 0x04, 100, 0, 2) && (sim800l_cntp_data.resp == 1))
			{
				sim800l_cntp_data.resp = 0;
				if(sim800l_send("AT+CNTP\r\n\0", 0x04, 100, 0, 2))
				{
					if(sim800l_cntp_data.resp)
					{
						sim800l_cntp_data.resp = 0;
						HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
						time_stamp = HAL_GetTick();
						while((HAL_GetTick() - time_stamp) < 20000)
						{
							if(sim800l_cntp_data.stat == 1)
							{
								sim800l_cntp_data.stat = 0;
								sim800l_cntp_data.resp = 1;
								break;
							}
							else if(sim800l_cntp_data.stat == 2)
							{
								sim800l_cntp_data.stat = 0;
								break;
							}
						}
						sim800l_global_stat.reply_stat2 &= ~(0x80000000);
						HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
						break;
					}
				}
			}
		}
		HAL_Delay(1000);
	}
	if(sim800l_cntp_data.resp)
	{
		sim800l_cntp_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_cclk_ask(void)
{
	uint8_t at_try = 0;
	while(at_try < 3)
	{
		at_try++;
		if(sim800l_send("AT+CCLK?\r\n\0", 0x08, 100, 0, 2))
		{
			break;
		}
		HAL_Delay(1000);
	}
	if(sim800l_cclk_data.resp)
	{
		sim800l_cclk_data.resp = 0;
		return 1;
	}
	return 0;
}
_Bool sim800l_sapbr_clear(void)
{
	uint8_t at_try = 0;
	uint32_t time_stamp = 0;
	while(at_try < 3)
	{
		at_try++;
		if(sim800l_send("AT+SAPBR=0,1\r\n\0", 0x01, 100, 0, 2))
		{
			HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
			time_stamp = HAL_GetTick();
			while((HAL_GetTick() - time_stamp) < 70000)
			{
				if(sim800l_atOK_read() || (sim800l_sapbr_data.resp == 1))
				{
					sim800l_sapbr_data.resp = 1;
					sim800l_atOK_clear_flag();
					break;
				}
				else if(sim800l_atERROR_read())
				{
					sim800l_atERROR_clear_flag();
					break;
				}
			}
			HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
			break;
		}
		HAL_Delay(1000);
	}
	if(sim800l_sapbr_data.resp)
	{
		sim800l_sapbr_data.resp = 0;
		return 1;
	}
	return 0;
}
void sim800l_reply_list(uint16_t element_index)
{
	uint8_t* ptr = NULL;
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "\r\n+IPD,")) != NULL)
	{
		sim800l_global_stat.reply_stat |= 0x01;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT\r\r\nOK\r\n")) != NULL)
	{
		memset(ptr, 0xff, 9);
		sim800l_global_stat.reply_stat |= 0x02;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "ATE1\r\r\nOK\r\n")) != NULL)
	{
		memset(ptr, 0xff, 11);
		sim800l_global_stat.reply_stat |= 0x04;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CGDCONT")) != NULL)
	{
		sim800l_cgdcont_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x08;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CSQ")) != NULL)
	{
		sim800l_csq_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x10;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CSTT")) != NULL)
	{
		if(sim800l_cstt_handler(ptr))
		{
			sim800l_global_stat.reply_stat |= 0x20;
		}
		else
		{
			sim800l_global_stat.reply_stat &= ~(0x20);
		}
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CIPSTATUS\r\r\nOK\r\n\r\nSTATE:")) != NULL)
	{
		sim800l_cipstatus_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x100;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CIPSHUT\r")) != NULL)
	{
		memset(ptr, 0xff, 11);
		sim800l_global_stat.reply_stat |= 0x200;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CGATT=1\r")) != NULL)
	{
		sim800l_cgatt_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x400;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CIICR\r")) != NULL)
	{
		sim800l_ciicr_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x800;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CREG")) != NULL)
	{
		sim800l_creg_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x1000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CIFSR")) != NULL)
	{
		sim800l_cifsr_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x2000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CIPHEAD=1\r")) != NULL)
	{
		sim800l_ciphead_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x4000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CGREG")) != NULL)
	{
		sim800l_cgreg_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x8000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CIPSHOWTP")) != NULL)
	{
		sim800l_cipshowtp_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x10000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CIPTKA")) != NULL)
	{
		sim800l_ciptka_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x20000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CIPQSEND")) != NULL)
	{
		sim800l_cipqsend_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x30000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CIPMUX")) != NULL)
	{
		sim800l_cipmux_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x40000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CIPSCONT")) != NULL)
	{
		sim800l_cipscont_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x80000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT&W0")) != NULL)
	{
		sim800l_atw_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x100000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CIPSTART")) != NULL)
	{
		sim800l_cipstart_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x200000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CIPSEND\r\r\n>")) != NULL)
	{
		memset(ptr, 0xff, 14);
		sim800l_global_stat.reply_stat |= 0x400000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+SAPBR")) != NULL)
	{
		sim800l_sapbr_handler(ptr);
		sim800l_global_stat.reply_stat2 |= 0x01;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CNTPCID")) != NULL)
	{
		sim800l_cntpcid_handler(ptr);
		sim800l_global_stat.reply_stat2 |= 0x02;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CNTP")) != NULL)
	{
		sim800l_cntp_handler(ptr);
		sim800l_global_stat.reply_stat2 |= 0x04;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "AT+CCLK")) != NULL)
	{
		sim800l_cclk_handler(ptr);
		sim800l_global_stat.reply_stat2 |= 0x08;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "\r\nSEND FAIL\r\n")) != NULL)
	{
		memset(ptr, 0xff, 13);
		sim800l_global_stat.reply_stat |= 0x800000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "\r\nSEND OK\r\n")) != NULL)
	{
		memset(ptr, 0xff, 11);
		sim800l_global_stat.reply_stat |= 0x1000000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "\r\n+PDP: DEACT\r\n")) != NULL)
	{
		memset(ptr, 0xff, 15);
		sim800l_cipstart_data.tcpStat = 3;
		sim800l_global_stat.reply_stat |= 0x2000000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "\r\nCLOSED\r\n")) != NULL)
	{
		memset(ptr, 0xff, 10);
		sim800l_cipstart_data.tcpStat = 2;
		sim800l_global_stat.reply_stat |= 0x4000000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "\r\nSTATE: ")) != NULL)
	{
		sim800l_tcpfailed_handler(ptr);
		sim800l_global_stat.reply_stat |= 0x8000000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "\r\nALREADY CONNECT\r\n")) != NULL)
	{
		memset(ptr, 0xff, 19);
		sim800l_global_stat.reply_stat |= 0x10000000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "\r\nCONNECT OK\r\n")) != NULL)
	{
		memset(ptr, 0xff, 14);
		sim800l_global_stat.reply_stat |= 0x10000000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "\r\nSHUT OK\r\n")) != NULL)
	{
		memset(ptr, 0xff, 11);
		sim800l_global_stat.reply_stat |= 0x20000000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "\r\nOK\r\n")) != NULL)
	{
		memset(ptr, 0xff, 6);
		sim800l_global_stat.reply_stat |= 0x40000000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "\r\nERROR\r\n")) != NULL)
	{
		memset(ptr, 0xff, 9);
		sim800l_global_stat.reply_stat |= 0x80000000;
	}
	if((ptr = (uint8_t*) strstr((const char*) &gsm_rx_buffer[element_index], "\r\n+CNTP: ")) != NULL)
	{
		sim800l_ntpstat_handler(ptr);
		sim800l_global_stat.reply_stat2 |= 0x80000000;
	}
}
void sim800l_echoOn(void)
{
	sim800l_debug_print("echo          : ");
	uint8_t try = 0;
	while(1)
	{
		if(sim800l_commandEcho_on())
		{
			sim800l_error_stat &= ~(ECHO_ON_ERROR);
			sim800l_debug_print("success\r\n\0");
			break;
		}
		else
		{
			try++;
			if (try > 3)
			{
				sim800l_error_stat |= ECHO_ON_ERROR;
				sim800l_debug_print("failed\r\n\0");
				break;
			}
			sim800l_debug_print("restart modem\r\n\0");
			sim800l_force_restart();
			sim800l_debug_print("echo          : ");
		}
	}
	HAL_Delay(100);
}
void sim800l_pdpSet(void)
{
	if (!sim800l_error_stat)
	{
		sim800l_debug_print("pdpSet        : ");
		uint8_t try = 0;
		while (1)
		{
			if (sim800l_pdpContext_set())
			{
				sim800l_error_stat &= ~(PDPSET_ERROR);
				sim800l_debug_print("success\r\n\0");
				break;
			}
			else
			{
				try++;
				if (try > 3)
				{
					sim800l_error_stat |= PDPSET_ERROR;
					sim800l_debug_print("failed\r\n\0");
					break;
				}
				sim800l_debug_print("restart modem\r\n\0");
				sim800l_force_restart();
				sim800l_echoOn();
				if (sim800l_error_stat > 0)
				{
					break;
				}
				sim800l_debug_print("pdpSet        : ");
			}
		}
		HAL_Delay(100);
	}
}
void sim800l_cregCheck(void)
{
	if (!sim800l_error_stat)
	{
		sim800l_debug_print("creg          : ");
		uint32_t timeout = 0;
		timeout = HAL_GetTick();
		while (1)
		{
			if (((HAL_GetTick() - timeout) > 120000) || !sim800l_creg_check())
			{
				sim800l_debug_print("restart modem\r\n\0");
				HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
				sim800l_force_restart();
				sim800l_echoOn();
				sim800l_pdpSet();
				if (sim800l_error_stat > 0)
				{
					break;
				}
				sim800l_debug_print("creg          : ");
				timeout = HAL_GetTick();
			}
			if(sim800l_cregData_read())
			{
				break;
			}
			HAL_Delay(750);
			HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
			HAL_Delay(50);
			HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
		}
		HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
		sim800l_debug_print("registered to network\r\n\0");
		HAL_Delay(100);
	}
}
void sim800l_cgregCheck(void)
{
	if (!sim800l_error_stat)
	{
		sim800l_debug_print("cgreg         : ");
		uint32_t timeout = 0;
		timeout = HAL_GetTick();
		while (1)
		{
			if (((HAL_GetTick() - timeout) > 120000) || !sim800l_cgreg_check())
			{
				sim800l_debug_print("restart modem\r\n\0");
				HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
				sim800l_force_restart();
				sim800l_echoOn();
				sim800l_pdpSet();
				sim800l_cregCheck();
				if (sim800l_error_stat > 0)
				{
					break;
				}
				sim800l_debug_print("cgreg         : ");
				timeout = HAL_GetTick();
			}
			if(sim800l_cgregData_read())
			{
				break;
			}
			HAL_Delay(50);
			HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
			HAL_Delay(750);
			HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
		}
		HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
		sim800l_debug_print("registered to network\r\n\0");
		HAL_Delay(100);
	}
}
void sim800l_cipheadSet(void)
{
	if (!sim800l_error_stat)
	{
		sim800l_debug_print("cipheadSet    : ");
		uint8_t try = 0;
		while (1)
		{
			if (sim800l_ciphead_set())
			{
				sim800l_error_stat &= ~(CIPHEAD_ERROR);
				sim800l_debug_print("success\r\n\0");
				break;
			}
			else
			{
				try++;
				if (try > 3)
				{
					sim800l_error_stat |= CIPHEAD_ERROR;
					sim800l_debug_print("failed\r\n\0");
					break;
				}
				sim800l_debug_print("restart modem\r\n\0");
				sim800l_force_restart();
				sim800l_echoOn();
				sim800l_pdpSet();
				sim800l_cregCheck();
				sim800l_cgregCheck();
				if (sim800l_error_stat > 0)
				{
					break;
				}
				sim800l_debug_print("cipheadSet    : ");
			}
		}
		HAL_Delay(100);
	}
}
void sim800l_cipshowtpSet(void)
{
	if (!sim800l_error_stat)
	{
		sim800l_debug_print("cipshowtpSet  : ");
		uint8_t try = 0;
		while (1)
		{
			if (sim800l_cipshowtp_set())
			{
				sim800l_error_stat &= ~(CIPSHOWTP_ERROR);
				sim800l_debug_print("success\r\n\0");
				break;
			}
			else
			{
				try++;
				if (try > 3)
				{
					sim800l_error_stat |= CIPSHOWTP_ERROR;
					sim800l_debug_print("failed\r\n\0");
					break;
				}
				sim800l_debug_print("restart modem\r\n\0");
				sim800l_force_restart();
				sim800l_echoOn();
				sim800l_pdpSet();
				sim800l_cregCheck();
				sim800l_cgregCheck();
				sim800l_cipheadSet();
				if (sim800l_error_stat > 0)
				{
					break;
				}
				sim800l_debug_print("cipshowtpSet  : ");
			}
		}
		HAL_Delay(100);
	}
}
void sim800l_cipqsendSet(void)
{
	if (!sim800l_error_stat)
	{
		sim800l_debug_print("cipqsendSet   : ");
		uint8_t try = 0;
		while (1)
		{
			if (sim800l_cipqsend_set())
			{
				sim800l_error_stat &= ~(CIPQSEND_ERROR);
				sim800l_debug_print("success\r\n\0");
				break;
			}
			else
			{
				try++;
				if (try > 3)
				{
					sim800l_error_stat |= CIPQSEND_ERROR;
					sim800l_debug_print("failed\r\n\0");
					break;
				}
				sim800l_debug_print("restart modem\r\n\0");
				sim800l_force_restart();
				sim800l_echoOn();
				sim800l_pdpSet();
				sim800l_cregCheck();
				sim800l_cgregCheck();
				sim800l_cipheadSet();
				sim800l_cipshowtpSet();
				if (sim800l_error_stat > 0)
				{
					break;
				}
				sim800l_debug_print("cipqsendSet   : ");
			}
		}
		HAL_Delay(100);
	}
}
void sim800l_cipmuxSet(void)
{
	if (!sim800l_error_stat)
	{
		sim800l_debug_print("cipmuxSet     : ");
		uint8_t try = 0;
		if(sim800l_cipmux_ask())
		{
			HAL_Delay(100);
			if(sim800l_cipmux_read())
			{
				HAL_Delay(100);
				while (1)
				{
					if (sim800l_cipmux_set())
					{
						sim800l_error_stat &= ~(CIPMUX_ERROR);
						sim800l_debug_print("success\r\n\0");
						break;
					}
					else
					{
						try++;
						if (try > 3)
						{
							sim800l_error_stat |= CIPMUX_ERROR;
							sim800l_debug_print("failed\r\n\0");
							break;
						}
						sim800l_debug_print("restart modem\r\n\0");
						sim800l_force_restart();
						sim800l_echoOn();
						sim800l_pdpSet();
						sim800l_cregCheck();
						sim800l_cgregCheck();
						sim800l_cipheadSet();
						sim800l_cipshowtpSet();
						sim800l_cipqsendSet();
						if (sim800l_error_stat > 0)
						{
							break;
						}
						sim800l_debug_print("cipmuxSet     : ");
					}
				}
				HAL_Delay(100);
			}
			else
			{
				sim800l_error_stat &= ~(CIPMUX_ERROR);
				sim800l_debug_print("already set\r\n\0");
			}
		}
		else
		{
			sim800l_error_stat |= CIPMUX_ERROR;
			sim800l_debug_print("failed\r\n\0");
		}
	}
}
void sim800l_ciptkaSet(void)
{
	if (!sim800l_error_stat)
	{
		sim800l_debug_print("ciptkaSet     : ");
		uint8_t try = 0;
		if(sim800l_ciptka_ask())
		{
			HAL_Delay(100);
			if(sim800l_ciptka_read())
			{
				HAL_Delay(100);
				while (1)
				{
					if (sim800l_ciptka_set())
					{
						sim800l_error_stat &= ~(CIPTKA_ERROR);
						sim800l_debug_print("success\r\n\0");
						break;
					}
					else
					{
						try++;
						if (try > 3)
						{
							sim800l_error_stat |= CIPTKA_ERROR;
							sim800l_debug_print("failed\r\n\0");
							break;
						}
						sim800l_debug_print("restart modem\r\n\0");
						sim800l_force_restart();
						sim800l_echoOn();
						sim800l_pdpSet();
						sim800l_cregCheck();
						sim800l_cgregCheck();
						sim800l_cipheadSet();
						sim800l_cipshowtpSet();
						sim800l_cipqsendSet();
						sim800l_cipmuxSet();
						if (sim800l_error_stat > 0)
						{
							break;
						}
						sim800l_debug_print("ciptkaSet     : ");
					}
				}
				HAL_Delay(100);
			}
			else
			{
				sim800l_error_stat &= ~(CIPTKA_ERROR);
				sim800l_debug_print("already set\r\n\0");
			}
		}
		else
		{
			sim800l_error_stat |= CIPTKA_ERROR;
			sim800l_debug_print("failed\r\n\0");
		}
	}
}
void sim800l_cgattSet(void)
{
	if (!sim800l_error_stat)
	{
		sim800l_debug_print("cgatt         : ");
		uint8_t try = 0;
		while (1)
		{
			if (sim800l_cgatt_set())
			{
				sim800l_error_stat &= ~(CGATT_ERROR);
				sim800l_debug_print("success\r\n\0");
				break;
			}
			else
			{
				try++;
				if (try > 3)
				{
					sim800l_error_stat |= CGATT_ERROR;
					sim800l_debug_print("failed\r\n\0");
					break;
				}
				sim800l_debug_print("restart modem\r\n\0");
				sim800l_force_restart();
				sim800l_echoOn();
				sim800l_pdpSet();
				sim800l_cregCheck();
				sim800l_cgregCheck();
				sim800l_cipheadSet();
				sim800l_cipshowtpSet();
				sim800l_cipqsendSet();
				sim800l_cipmuxSet();
				sim800l_ciptkaSet();
				if (sim800l_error_stat > 0)
				{
					break;
				}
				sim800l_debug_print("cgatt         : ");
			}
		}
		HAL_Delay(100);
	}
}
void sim800l_csttSet(void)
{
	if (!sim800l_error_stat)
	{
		sim800l_debug_print("cstt          : ");
		uint8_t try = 0;
		while (1)
		{
			if (sim800l_cstt_set())
			{
				sim800l_error_stat &= ~(CSTT_ERROR);
				sim800l_debug_print("success\r\n\0");
				break;
			}
			else
			{
				try++;
				if (try > 3)
				{
					sim800l_error_stat |= CSTT_ERROR;
					sim800l_debug_print("failed\r\n\0");
					break;
				}
				sim800l_debug_print("restart modem\r\n\0");
				sim800l_force_restart();
				sim800l_echoOn();
				sim800l_pdpSet();
				sim800l_cregCheck();
				sim800l_cgregCheck();
				sim800l_cipheadSet();
				sim800l_cipshowtpSet();
				sim800l_cipqsendSet();
				sim800l_cipmuxSet();
				sim800l_ciptkaSet();
				sim800l_cgattSet();
				if (sim800l_error_stat > 0)
				{
					break;
				}
				sim800l_debug_print("cstt          : ");
			}
		}
		HAL_Delay(100);
	}
}
void sim800l_ciicrSet(void)
{
	if (!sim800l_error_stat)
	{
		sim800l_debug_print("ciicr         : ");
		uint8_t try = 0;
		while (1)
		{
			if (sim800l_ciicr_set())
			{
				sim800l_error_stat &= ~(CIICR_ERROR);
				sim800l_debug_print("success\r\n\0");
				break;
			}
			else
			{
				try++;
				if (try > 3)
				{
					sim800l_error_stat |= CIICR_ERROR;
					sim800l_debug_print("failed\r\n\0");
					break;
				}
				sim800l_debug_print("restart modem\r\n\0");
				sim800l_force_restart();
				sim800l_echoOn();
				sim800l_pdpSet();
				sim800l_cregCheck();
				sim800l_cgregCheck();
				sim800l_cipheadSet();
				sim800l_cipshowtpSet();
				sim800l_cipqsendSet();
				sim800l_cipmuxSet();
				sim800l_ciptkaSet();
				sim800l_cgattSet();
				sim800l_csttSet();
				if (sim800l_error_stat > 0)
				{
					break;
				}
				sim800l_debug_print("ciicr         : ");
			}
		}
		HAL_Delay(100);
	}
}
void sim800l_cifsrSet(void)
{
	if (!sim800l_error_stat)
	{
		sim800l_debug_print("cifsr         : ");
		uint8_t try = 0;
		while (1)
		{
			if (sim800l_cifsr_set())
			{
				sim800l_error_stat &= ~(CIFSR_ERROR);
				sim800l_debug_print("success\r\n\0");
				break;
			}
			else
			{
				try++;
				if (try > 3)
				{
					sim800l_error_stat |= CIFSR_ERROR;
					sim800l_debug_print("failed\r\n\0");
					break;
				}
				sim800l_debug_print("restart modem\r\n\0");
				sim800l_force_restart();
				sim800l_echoOn();
				sim800l_pdpSet();
				sim800l_cregCheck();
				sim800l_cgregCheck();
				sim800l_cipheadSet();
				sim800l_cipshowtpSet();
				sim800l_cipqsendSet();
				sim800l_cipmuxSet();
				sim800l_ciptkaSet();
				sim800l_cgattSet();
				sim800l_csttSet();
				sim800l_ciicrSet();
				if (sim800l_error_stat > 0)
				{
					break;
				}
				sim800l_debug_print("cifsr         : ");
			}
		}
		HAL_Delay(100);
	}
}
void sim800l_saveTCP_profile(void)
{
	sim800l_cipscont_set();
	HAL_Delay(100);
}
void sim800l_save_profile(void)
{
	sim800l_atw_set();
	HAL_Delay(100);
}
_Bool sim800l_cipsendData(const char* message, uint8_t message_len)
{
	uint8_t status = 4;
	if (!sim800l_error_stat)
	{
		sim800l_debug_print("cipsend       : ");
		uint8_t try = 0;
		while(1)
		{
			try++;
			if (try > 2 || sim800l_error_stat > 0)
			{
				sim800l_error_stat |= CIPSENDDATA_ERROR;
				sim800l_debug_print("failed\r\n\0");
				break;
			}
			status = sim800l_cipsend_data(message, message_len);
			if(status == 1)
			{
				sim800l_error_stat &= ~(CIPSENDDATA_ERROR);
				sim800l_debug_print("success\r\n\0");
				break;
			}
			else
			{
				HAL_Delay(1000);
				continue;
			}
		}
	}
	if(status != 1)
	{
		status = 0;
	}
	return status;
}
void sim800l_tcpStart(void)
{
	if (!sim800l_error_stat)
	{
		sim800l_debug_print("cipstart      : ");
		uint8_t try = 0;
		while (1)
		{
			if (sim800l_cipstart_set())
			{
				sim800l_error_stat &= ~(TCP_START_ERROR);
				sim800l_debug_print("success\r\n\0");
				break;
			}
			else
			{
				try++;
				if (try > 3)
				{
					sim800l_error_stat |= TCP_START_ERROR;
					sim800l_debug_print("failed\r\n\0");
					break;
				}
				sim800l_debug_print("restart modem\r\n\0");
				sim800l_force_restart();
				sim800l_echoOn();
				sim800l_pdpSet();
				sim800l_cregCheck();
				sim800l_cgregCheck();
				sim800l_cipheadSet();
				sim800l_cipshowtpSet();
				sim800l_cipqsendSet();
				sim800l_cipmuxSet();
				sim800l_ciptkaSet();
				sim800l_cgattSet();
				sim800l_csttSet();
				sim800l_ciicrSet();
				if (sim800l_error_stat > 0)
				{
					break;
				}
				sim800l_debug_print("cipstart      : ");
			}
		}
		HAL_Delay(100);
	}
}
void sim800l_ntpSync(void)
{
	if (!sim800l_error_stat && !sim800l_cclk_data.sync)
	{
		sim800l_debug_print("ntp sync      : ");
		uint8_t try = 0;
		while (1)
		{
			if (sim800l_sapbr_set())
			{
				HAL_Delay(2000);
				uint8_t numb_ntp_server = 0;
				while(numb_ntp_server < 9)
				{
					sim800l_cntp_data.try_count = numb_ntp_server;
					if(sim800l_ntp_set())
					{
						HAL_Delay(100);
						if(sim800l_cclk_ask())
						{
							HAL_Delay(100);
							if(sim800l_sapbr_clear())
							{
								sim800l_debug_print("success\r\n\0");
								numb_ntp_server = 10;
								break;
							}
						}
					}
					else
					{
						numb_ntp_server++;
					}
				}
				if(numb_ntp_server == 10)
				{
					break;
				}
			}
			try++;
			if (try > 1)
			{
				sim800l_debug_print("failed\r\n\0");
				break;
			}
			sim800l_debug_print("restart modem\r\n\0");
			sim800l_force_restart();
			sim800l_echoOn();
			sim800l_pdpSet();
			sim800l_cregCheck();
			sim800l_cgregCheck();
			sim800l_cipheadSet();
			sim800l_cipshowtpSet();
			sim800l_cipqsendSet();
			sim800l_cipmuxSet();
			sim800l_ciptkaSet();
			sim800l_cgattSet();
			if (sim800l_error_stat > 0)
			{
				break;
			}
			sim800l_debug_print("ntp sync      : ");
			HAL_Delay(1000);
		}
		HAL_Delay(100);
	}
}
void sim800l_reconnect(uint8_t state)
{
	sim800l_cipstart_data.tcpStat = 0;
	if(state)
	{
		sim800l_csttSet();
		sim800l_ciicrSet();
		sim800l_cifsrSet();
	}
	sim800l_tcpStart();
}
sim800l_cclk_data_t sim800l_get_cclk(void)
{
	sim800l_cclk_data.sync = 0;
	sim800l_cclk_ask();
	if(sim800l_cclk_data.sync == 0)
	{
		sim800l_cclk_data.date = 0;
		sim800l_cclk_data.month = 0;
		sim800l_cclk_data.year = 0;
		sim800l_cclk_data.second = 0;
		sim800l_cclk_data.minute = 0;
		sim800l_cclk_data.hour = 0;
		sim800l_cclk_data.timezone = 0;
		sim800l_cclk_data.resp = 0;
		sim800l_cclk_data.sync = 0;
	}
	return sim800l_cclk_data;
}

void sim800l_init(void)
{
	sim800l_error_stat = 0;
	sim800l_debug_print("Initializing SIM800l\r\n\0");
	sim800l_echoOn();
	sim800l_pdpSet();
	sim800l_cregCheck();
	sim800l_cgregCheck();
	sim800l_cipheadSet();
	sim800l_cipshowtpSet();
	sim800l_cipqsendSet();
	sim800l_cipmuxSet();
	sim800l_ciptkaSet();
	sim800l_cgattSet();
	sim800l_csttSet();
	sim800l_ciicrSet();
	sim800l_cifsrSet();
	sim800l_saveTCP_profile();
	sim800l_save_profile();
	sim800l_tcpStart();
	if(!sim800l_error_stat)
	{
		sim800l_debug_print("Initialize Completed\r\n\0");
	}
	else
	{
		uint8_t char_error_code[3] = {0};
		sim800l_cclk_data.sync = 0;
		snprintf((char*) &char_error_code[0], 3, "%x", sim800l_error_stat);
		sim800l_debug_print("Some error occurs when initializing, error code :\"");
		sim800l_debug_print((const char*) &char_error_code[0]);
		sim800l_debug_print("\"\r\n\0");
	}
}

void sim800l_error_init_check(void)
{
	if (sim800l_error_stat != 0)
	{
		if (sim800l_error_stat & ECHO_ON_ERROR)
		{
			sim800l_echoOn();
		}
		if (sim800l_error_stat & PDPSET_ERROR)
		{
			sim800l_pdpSet();
		}
		if (sim800l_error_stat & CIPHEAD_ERROR)
		{
			sim800l_cipheadSet();
		}
		if (sim800l_error_stat & CIPSHOWTP_ERROR)
		{
			sim800l_cipshowtpSet();
		}
		if (sim800l_error_stat & CIPQSEND_ERROR)
		{
			sim800l_cipqsendSet();
		}
		if (sim800l_error_stat & CIPMUX_ERROR)
		{
			sim800l_cipmuxSet();
		}
		if (sim800l_error_stat & CIPTKA_ERROR)
		{
			sim800l_ciptkaSet();
		}
		if (sim800l_error_stat & CGATT_ERROR)
		{
			sim800l_cgattSet();
		}
		if (sim800l_error_stat & CSTT_ERROR)
		{
			sim800l_csttSet();
		}
		if (sim800l_error_stat & CIICR_ERROR)
		{
			sim800l_ciicrSet();
		}
		if (sim800l_error_stat & CIFSR_ERROR)
		{
			sim800l_cifsrSet();
		}
	}
}
