#include "ina219.h"
extern I2C_HandleTypeDef hi2c1;
float ina219_read_shunt_voltage(void)
{
	int16_t shunt_voltage_data = 0;
	uint8_t ina219_shunt_voltage_register[2] = {0};
	HAL_I2C_Mem_Read(&hi2c1, INA219_Address, INA219_Shunt_Voltage, 1, &ina219_shunt_voltage_register[0], 2, 4);
	shunt_voltage_data = ((ina219_shunt_voltage_register[0]<<8) | ina219_shunt_voltage_register[1]);
	return (shunt_voltage_data * 0.01); // miliVolt
}
float ina219_read_bus_voltage(void)
{
	uint16_t bus_voltage_data = 0;
	uint8_t ina219_bus_voltage_register[2] = {0};
	HAL_I2C_Mem_Read(&hi2c1, INA219_Address, INA219_Bus_Voltage, 1, &ina219_bus_voltage_register[0], 2, 4);
	if(ina219_bus_voltage_register[1] & 0x01)
	{
		return 0;
	}
	else
	{
		if(ina219_bus_voltage_register[1] & 0x02)
		{
			bus_voltage_data = ((ina219_bus_voltage_register[0] << 5) | (ina219_bus_voltage_register[1] >> 3));
			return (bus_voltage_data * 4.0f); // miliVolt
		}
		return 0;
	}
}
float ina219_read_bus_power(void)
{
	int16_t power_data = 0;
	uint8_t ina219_power_register[2] = {0};
	HAL_I2C_Mem_Read(&hi2c1, INA219_Address, INA219_Power, 1, &ina219_power_register[0], 2, 4);
	power_data = ((ina219_power_register[0]<<8) | ina219_power_register[1]);
	// 0.0018310546875 is Power_LSB that equal to 20 * Current LSB, Watt
	return (power_data * 1.8310546875f); // miliWatt
}
float ina219_read_bus_current(void)
{
    int16_t current_data = 0;
    uint8_t ina219_current_register[2] = {0};
	HAL_I2C_Mem_Read(&hi2c1, INA219_Address, INA219_Current, 1, &ina219_current_register[0], 2, 4);
	current_data = ((ina219_current_register[0]<<8) | ina219_current_register[1]);
	// 0.000091552734375 is Current_LSB that equal to max expected current (3A) / 2^15, Ampere
	return (current_data * 0.091552734375f); // miliAmpere
}
ina219_data_t ina219_read(void)
{
	ina219_data_t ina219_data_temp =
	{ 0 };
	ina219_data_temp.shunt_voltage = ina219_read_shunt_voltage();
	ina219_data_temp.bus_current = ina219_read_bus_current();
	ina219_data_temp.bus_power = ina219_read_bus_power();
	ina219_data_temp.bus_voltage = ina219_read_bus_voltage();
	return ina219_data_temp;
}
void ina219_init(void)
{
    uint8_t ina219_config[3] = {INA219_Configuration, INA219_vConfiguration >> 8, (uint8_t) INA219_vConfiguration};
    uint8_t ina219_calib[3] = {INA219_Calibration, INA219_vCalibration >> 8, (uint8_t) INA219_vCalibration};
    uint8_t ina219_config_ver[2] = {0};
    uint8_t ina219_calib_ver[2] = {0};
    HAL_I2C_Master_Transmit(&hi2c1, INA219_Address, &ina219_config[0], 3, 3);
    HAL_I2C_Master_Transmit(&hi2c1, INA219_Address, &ina219_calib[0], 3, 3);
    HAL_I2C_Mem_Read(&hi2c1, INA219_Address, INA219_Configuration, 1, &ina219_config_ver[0], 2, 4);
    HAL_I2C_Mem_Read(&hi2c1, INA219_Address, INA219_Calibration, 1, &ina219_calib_ver[0], 2, 4);
}
