#include "stm32l071.h"
#include "time.h"
#include "string.h"
#include "stdio.h"
#include "MQTTPacket.h"
#include "ina219.h"
#include "l86.h"
#include "sim800l.h"

extern ADC_HandleTypeDef hadc;
extern RTC_HandleTypeDef hrtc;
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
//extern UART_HandleTypeDef hlpuart1;

typedef struct
{
	_Bool session;
	_Bool puback;
	_Bool pingreq;
} stm32l071_mqtt_stat_t;

float stm32l071_read_tempSensor(void);
uint32_t stm32l071_create_epoch(void);
RTC_TimeTypeDef stm32l071_get_TimeRTC(void);
RTC_DateTypeDef stm32l071_get_DateRTC(void);


RTC_AlarmTypeDef rtcAlarmm = { 0 };
stm32l071_mqtt_stat_t stm32l071_mqtt_stat = { 0 };
uint16_t TS_CAL1 = 0;
uint16_t TS_CAL2 = 0;
uint16_t payload_seq = 0;
_Bool rtcSync_stat = 0;


void stm32l071_init_tempSensor(void)
{
	uint16_t* a = (uint16_t*) 0x1ff8007a;
	uint16_t* b = (uint16_t*) 0x1ff8007e;
	TS_CAL1 = *a;
	TS_CAL2 = *b;
	HAL_ADC_Start(&hadc);
}
float stm32l071_read_tempSensor(void)
{
	float adc_val = (float) HAL_ADC_GetValue(&hadc);
	float internal_temp = (((100.0 / (((float) TS_CAL2) - ((float) TS_CAL1))) * (adc_val - TS_CAL1)) + 30.0);
	return internal_temp;
}
uint32_t stm32l071_create_epoch(void)
{
	struct tm rawData = { 0 };
	time_t epochData = 0;
	RTC_TimeTypeDef rawTime = { 0 };
	RTC_DateTypeDef rawDate = { 0 };
	rawTime = stm32l071_get_TimeRTC();
	rawDate = stm32l071_get_DateRTC();
	rawData.tm_year = ((rawDate.Year + 2000) - 1900);
	rawData.tm_mon = (rawDate.Month - 1);
	rawData.tm_mday = rawDate.Date;
	rawData.tm_hour = rawTime.Hours;
	rawData.tm_min = rawTime.Minutes;
	rawData.tm_sec = rawTime.Seconds;
	epochData = mktime(&rawData);
	return (uint32_t) epochData;
}
void stm32l071_create_json(uint8_t *payload)
{
	uint8_t payload_len = 0;
	ina219_data_t ina219_data = { 0 };
	parse_l86_data_t current_gps_data = { 0 };
	float uC_temp = 0;

	memset((void*) &payload[0], 0x00, 110);

	payload_seq++;
	uC_temp = stm32l071_read_tempSensor();
	ina219_data = ina219_read();
	current_gps_data = l86_get_gpsData();
	payload[0] = '{';
	if (rtcSync_stat)
	{
		uint32_t epoch = stm32l071_create_epoch();
		payload_len = strlen((const char*) &payload[0]);
		memcpy((void*) &payload[payload_len], (void*) "\"ts\":", 5);
		snprintf((char*) &payload[payload_len + 5], 11, "%ld", epoch);
		memcpy((void*) &payload[payload_len + 15], (void*) ",\0", 2);
	}
	if (current_gps_data.rmc)
	{
		payload_len = strlen((const char*) &payload[0]);
		memcpy((void*) &payload[payload_len], (void*) "\"1\":", 4);
		snprintf((char*) &payload[payload_len + 4], 11, "%10.7f", current_gps_data.lat);
		memcpy((void*) &payload[payload_len + 14], (void*) ",\0", 2);
		payload_len = strlen((const char*) &payload[0]);
		memcpy((void*) &payload[payload_len], (void*) "\"2\":", 4);
		snprintf((char*) &payload[payload_len + 4], 11, "%10.7f", current_gps_data.lon);
		memcpy((void*) &payload[payload_len + 14], (void*) ",\0", 2);
		payload_len = strlen((const char*) &payload[0]);
		memcpy((void*) &payload[payload_len], (void*) "\"3\":", 4);
		snprintf((char*) &payload[payload_len + 4], 4, "%3hi", current_gps_data.heading);
		memcpy((void*) &payload[payload_len + 7], (void*) ",\0", 2);
		payload_len = strlen((const char*) &payload[0]);
		memcpy((void*) &payload[payload_len], (void*) "\"4\":", 4);
		snprintf((char*) &payload[payload_len + 4], 4, "%3hi", current_gps_data.spd);
		memcpy((void*) &payload[payload_len + 7], (void*) ",\0", 2);
	}
	payload_len = strlen((const char*) &payload[0]);
	memcpy((void*) &payload[payload_len], (void*) "\"5\":", 4);
	snprintf((char*) &payload[payload_len + 4], 6, "%5.2f", uC_temp);
	memcpy((void*) &payload[payload_len + 9], (void*) ",\0", 2);
	payload_len = strlen((const char*) &payload[0]);
	memcpy((void*) &payload[payload_len], (void*) "\"6\":", 4);
	snprintf((char*) &payload[payload_len + 4], 7, "%6.1f", ina219_data.bus_voltage);
	memcpy((void*) &payload[payload_len + 10], (void*) ",\0", 2);
	payload_len = strlen((const char*) &payload[0]);
	memcpy((void*) &payload[payload_len], (void*) "\"7\":", 4);
	snprintf((char*) &payload[payload_len + 4], 7, "%6.1f", ina219_data.bus_power);
	memcpy((void*) &payload[payload_len + 10], (void*) ",\0", 2);
	payload_len = strlen((const char*) &payload[0]);
	memcpy((void*) &payload[payload_len], (void*) "\"0\":", 4);
	snprintf((char*) &payload[payload_len + 4], 7, "%6hu", payload_seq);
	memcpy((void*) &payload[payload_len + 10], (void*) "\0", 2);
	payload_len = strlen((const char*) &payload[0]);
	payload[payload_len] = '}';
}
uint8_t stm32l071_create_mqttpacket(uint8_t* payload, uint8_t* mqttpacket, uint8_t mqttpacket_len)
{
	uint8_t len = 0;
	if(stm32l071_mqtt_stat.session == 0)
	{
		MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
		data.clientID.cstring = clientid;
		data.username.cstring = accesstoken;
		data.keepAliveInterval = 60;
		data.cleansession = 1;
		len = MQTTSerialize_connect(mqttpacket, mqttpacket_len, &data);
		mqttpacket[len] = '\x1A';
		len++;
	}
	else
	{
		MQTTString topicString = MQTTString_initializer;
		uint8_t payloadlen = strlen((const char*) payload);
		topicString.cstring = mqtttopic;
		len += MQTTSerialize_publish((mqttpacket + len), mqttpacket_len, 0, 0, 0, 1, topicString, payload, payloadlen);
		mqttpacket[len] = '\x1A';
		len++;
	}
	return len;
}
void stm32l071_set_mqttsession(_Bool session_stat)
{
	if(session_stat == 1)
	{
		stm32l071_mqtt_stat.session = 1;
	}
	else
	{
		stm32l071_mqtt_stat.session = 0;
	}
}
RTC_TimeTypeDef stm32l071_get_TimeRTC(void)
{
	RTC_TimeTypeDef rtcTime = { 0 };
	HAL_RTC_GetTime(&hrtc, &rtcTime, RTC_FORMAT_BIN);
	return rtcTime;
}
RTC_DateTypeDef stm32l071_get_DateRTC(void)
{
	RTC_DateTypeDef rtcDate = { 0 };
	HAL_RTC_GetDate(&hrtc, &rtcDate, RTC_FORMAT_BIN);
	return rtcDate;
}
void stm32l071_set_RTCTimeDate(_Bool timesource)
{
	RTC_TimeTypeDef newTime = { 0 };
	RTC_DateTypeDef newDate = { 0 };
	_Bool status = 0;
	if(timesource == 1)
	{
		parse_l86_data_t current_gps_data = { 0 };
		current_gps_data = l86_get_gpsData();
		newTime.Hours = current_gps_data.hh;
		newTime.Minutes = current_gps_data.mi;
		newTime.Seconds = current_gps_data.ss;
		newTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
		newTime.StoreOperation = RTC_STOREOPERATION_RESET;
		newDate.WeekDay = RTC_WEEKDAY_MONDAY;
		newDate.Month = current_gps_data.mo;
		newDate.Date = current_gps_data.dd;
		newDate.Year = current_gps_data.yy;
		status = 1;
	}
	else if(timesource == 0)
	{
		sim800l_cclk_data_t cclkData = { 0 };
		cclkData = sim800l_get_cclk();
		if(cclkData.sync != 0)
		{
			newTime.Hours = cclkData.hour;
			newTime.Minutes = cclkData.minute;
			newTime.Seconds = cclkData.second;
			newTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
			newTime.StoreOperation = RTC_STOREOPERATION_RESET;
			newDate.WeekDay = RTC_WEEKDAY_MONDAY;
			newDate.Month = cclkData.month;
			newDate.Date = cclkData.date;
			newDate.Year = cclkData.year;
			status = 1;
		}
	}
	if(status == 1 && HAL_RTC_SetDate(&hrtc, &newDate, RTC_FORMAT_BIN) == HAL_OK && HAL_RTC_SetTime(&hrtc, &newTime, RTC_FORMAT_BIN) == HAL_OK)
	{
		rtcSync_stat = 1;
	}
	else
	{
		rtcSync_stat = 0;
	}
}

void stm32l071_clear_backupreg(uint8_t bits)
{
	CLEAR_BIT(RTC->BKP0R, 0x01);
}

_Bool stm32l071_read_backupreg(void)
{
	if((READ_BIT(RTC->BKP0R, 0x01)) == 0x01)
	{
		return 1;
	}
	return 0;
}

void stm32l071_check_ophour(void)
{
	RTC_DateTypeDef currDate = { 0 };
	RTC_TimeTypeDef currTime = { 0 };
	RTC_AlarmTypeDef nextAlarm = { 0 };
	_Bool sleep_now = 0;
	HAL_RTC_GetDate(&hrtc, &currDate, RTC_FORMAT_BIN);
	HAL_RTC_GetTime(&hrtc, &currTime, RTC_FORMAT_BIN);
	if(currDate.WeekDay == 7)
	{
		nextAlarm.AlarmTime.Hours = 6;
		nextAlarm.AlarmTime.Minutes = 0;
		nextAlarm.AlarmTime.Seconds = 0;
		nextAlarm.AlarmTime.SubSeconds = 0;
		nextAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
		nextAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
		nextAlarm.AlarmMask = RTC_ALARMMASK_NONE;
		nextAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_NONE;
		nextAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_WEEKDAY;
		nextAlarm.AlarmDateWeekDay = 1;
		nextAlarm.Alarm = RTC_ALARM_A;
		HAL_RTC_SetAlarm_IT(&hrtc, &nextAlarm, RTC_FORMAT_BIN);
		sleep_now = 1;
	}
	else
	{
		if((currTime.Hours >= 9 && currTime.Minutes >= 10) && (currTime.Hours < 20))
		{
			nextAlarm.AlarmTime.Hours = 20;
			nextAlarm.AlarmTime.Minutes = 0;
			nextAlarm.AlarmTime.Seconds = 0;
			nextAlarm.AlarmTime.SubSeconds = 0;
			nextAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
			nextAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
			nextAlarm.AlarmMask = RTC_ALARMMASK_NONE | RTC_ALARMMASK_DATEWEEKDAY;
			nextAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_NONE;
			nextAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_WEEKDAY;
			nextAlarm.AlarmDateWeekDay = 1;
			nextAlarm.Alarm = RTC_ALARM_A;
			HAL_RTC_SetAlarm_IT(&hrtc, &nextAlarm, RTC_FORMAT_BIN);
			rtcSync_stat = 1;
			sleep_now = 0;
		}
		else
		{
			nextAlarm.AlarmTime.Hours = 9;
			nextAlarm.AlarmTime.Minutes = 10;
			nextAlarm.AlarmTime.Seconds = 0;
			nextAlarm.AlarmTime.SubSeconds = 0;
			nextAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
			nextAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
			nextAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_NONE;
			nextAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_WEEKDAY;
			nextAlarm.AlarmDateWeekDay = 1;
			nextAlarm.Alarm = RTC_ALARM_A;
			if(currDate.WeekDay == 6)
			{
				nextAlarm.AlarmMask = RTC_ALARMMASK_NONE;
				HAL_RTC_SetAlarm_IT(&hrtc, &nextAlarm, RTC_FORMAT_BIN);
				sleep_now = 1;
			}
			else
			{
				nextAlarm.AlarmMask = RTC_ALARMMASK_NONE | RTC_ALARMMASK_DATEWEEKDAY;
				HAL_RTC_SetAlarm_IT(&hrtc, &nextAlarm, RTC_FORMAT_BIN);
				sleep_now = 1;
			}
		}
	}
	if(sleep_now == 1)
	{
		SET_BIT(RTC->BKP0R, 0x02);
		stm32l071_enter_standby();
	}
}

void stm32l071_enter_standby(void)
{
	for(uint8_t cnt = 0; cnt < 50; cnt++)
	{
	  HAL_GPIO_TogglePin(LED2_GPIO_Port, LED2_Pin);
	  HAL_Delay(50);
	}
	HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(IO2HDR_GPIO_Port, IO2HDR_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(EN3V0_GPIO_Port, EN3V0_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOC, EN3V3_swGPS_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(swMDM_GPIO_Port, swMDM_Pin, GPIO_PIN_SET);
	HAL_UART_Abort_IT(&huart1);
	HAL_UART_Abort_IT(&huart2);
	HAL_ADC_Stop(&hadc);
	SET_BIT(PWR->CR, PWR_CR_CWUF);
	CLEAR_BIT(RTC->ISR, RTC_ISR_ALRAF);
	HAL_PWR_EnterSTANDBYMode();
}

//void STM32L071_LPUART_Send_RTC(void)
//{
//	RTC_TimeTypeDef Current_Time = {0};
//	RTC_DateTypeDef Current_Date = {0};
//	uint8_t TimeDate_Data[20] = {0};
//	HAL_RTC_GetDate(&hrtc, &Current_Date, RTC_FORMAT_BIN);
//	HAL_RTC_GetTime(&hrtc, &Current_Time, RTC_FORMAT_BIN);
//	HAL_UART_Transmit(&hlpuart1, &TimeDate_Data[0], strlen((const char*) TimeDate_Data), 1000);
//}
