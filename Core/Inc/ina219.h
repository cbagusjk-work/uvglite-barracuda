#ifndef __INA219_H
#define __INA219_H
#ifdef __cplusplus
extern "C" {
#endif
#include  "main.h"
#define 	INA219_Configuration	0x00
#define 	INA219_Shunt_Voltage	0x01
#define 	INA219_Bus_Voltage		0x02
#define 	INA219_Power			0x03
#define 	INA219_Current		    0x04
#define 	INA219_Calibration		0x05
#define 	INA219_Address  		0x40 << 1
#define 	INA219_vConfiguration	0x0C47
#define 	INA219_Real_vCalib		0x45E7
#define 	INA219_vCalibration		INA219_Real_vCalib
typedef struct
{
	float shunt_voltage;
	float bus_current;
	float bus_power;
	float bus_voltage;
} ina219_data_t;
void  		ina219_init(void);
float 		ina219_read_shunt_voltage(void);
float   	ina219_read_bus_current(void);
float 		ina219_read_bus_power(void);
float 		ina219_read_bus_voltage(void);
ina219_data_t ina219_read(void);
#ifdef __cplusplus
}
#endif
#endif
