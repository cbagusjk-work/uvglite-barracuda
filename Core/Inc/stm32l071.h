#include "main.h"

void stm32l071_enter_standby(void);
void stm32l071_set_backupreg(uint8_t bits);
void stm32l071_clear_backupreg(uint8_t bits);
void stm32l071_init_tempSensor(void);
void stm32l071_create_json(uint8_t *payload);
void stm32l071_setFlag_rtcSync(_Bool statFlag);
void stm32l071_set_RTCTimeDate(_Bool timesource);
void stm32l071_set_mqttsession(_Bool session_stat);
void stm32l071_check_ophour(void);
void STM32L071_LPUART_Send_RTC(void);

_Bool stm32l071_read_backupreg(void);

uint8_t stm32l071_create_mqttpacket(uint8_t* payload, uint8_t* mqttpacket, uint8_t mqttpacket_len);




