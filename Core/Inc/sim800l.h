#ifndef __SIM800_L_H
#define __SIM800_L_H

#ifdef __cplusplus
extern "C" {
#endif
	
#include "main.h"

#define endpoint_connect "AT+CIPSTART=\"TCP\",\"13.229.197.6\",1883\r\n\0"
#define clientid "UVGLite_9"
#define accesstoken "omlfypPuancjog1zx3fq"
#define mqtttopic "v1/devices/me/telemetry"

/* UVG Access Token
 *  UVGLite_#1 IC4t4Z05vYxPZqi7lkBF L80 	12
 *  UVGLite_#2 cC1pvx2pacBH3g5YSj4J	L86		13
 *  UVGLite_#3 59y8X7ck5yGBBI8e1ncu	L80R	12
 *  UVGLite_#4 ckKHw7ytl8gL6erwJuA7	L80R	12
 *  UVGLite_#5 7O5BO2a808Q2ZPMagipq	L80		12	non-patch + fan
 *  UVGLite_#6 AcdwXCdB7TBjhQ6Arur2	L80		12	non-patch + fan
 *  UVGLite_#7 va0eFfuj0TFnK2gzriSM	L80		12	non-patch
 *  UVGLite_#8 1M01gtrOt2u68htuIjfI	L80		12	non-patch
 *  UVGLite_#9 omlfypPuancjog1zx3fq	L80		12
 */

enum sim800l_init_error
{
	INIT_OK = 0x00,
	ECHO_ON_ERROR = 0x01,
	PDPSET_ERROR = 0x02,
	CREG_ERROR = 0x04,
	CGREG_ERROR = 0x08,
	CIPHEAD_ERROR = 0x10,
	CIPSHOWTP_ERROR = 0x20,
	CIPQSEND_ERROR = 0x40,
	CIPMUX_ERROR = 0x80,
	CIPTKA_ERROR = 0x100,
	CGATT_ERROR = 0x200,
	NTPSYNC_ERROR = 0x400,
	CSTT_ERROR = 0x800,
	CIICR_ERROR = 0x1000,
	CIFSR_ERROR = 0x2000,
	TCP_START_ERROR = 0x4000,
	CIPSENDDATA_ERROR = 0x8000,
};

typedef struct
{
	uint8_t date;
	uint8_t month;
	uint8_t year;
	uint8_t second;
	uint8_t minute;
	uint8_t hour;
	uint8_t timezone;
	uint8_t resp;
	_Bool sync;
} sim800l_cclk_data_t;

sim800l_cclk_data_t sim800l_get_cclk(void);
uint8_t sim800l_data_available(void);
uint8_t sim800l_tcpstat_read(void);
uint16_t sim800l_errorcode_read(void);

void sim800l_init(void);
void sim800l_start_rx(void);
void sim800l_rx_handler(void);
void sim800l_to_2ndbuff(void);
void sim800l_2ndbuff_process(void);
void sim800l_startup(void);
void sim800l_error_init_check(void);
void sim800l_pdpdeact_react(void);
void sim800l_tcp_reconnect(void);
void sim800l_ntpSync(void);

_Bool sim800l_cipsendData(const char* message, uint8_t message_len);
_Bool sim800l_mqttsession_read(void);
_Bool sim800l_ntpSync_read(void);

#ifdef __cplusplus
}
#endif

#endif
