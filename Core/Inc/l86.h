#ifndef __L86_H
#define __L86_H

#ifdef __cplusplus
extern "C" {
#endif
	
#include "main.h"

enum init_error_section
{
	DISABLE_NMEA_OUTPUT = 0x01,
	DISABLE_GPTXT = 0x02,
	CHANGE_BAUD_115200 = 0x04,
	POS_FIX_INTERVAL = 0x08,
	DGPS_MODE = 0x10,
	SBAS_ENABLE = 0x20,
	RMC_OUTPUT = 0x40,
	SEARCH_MODE = 0x80,
	SPEED_THRS = 0x100,
	EASY_ENABLE = 0x200,
};

typedef struct
{
	float lat;
	float lon;
	_Bool rmc;
	int32_t time;
	int32_t date;
	int16_t spd;
	int16_t heading;
	uint8_t chksum;
	uint8_t hh;
	uint8_t mi;
	uint8_t ss;
	uint8_t	yy;
	uint8_t mo;
	uint8_t	dd;
	uint8_t ww;
} parse_l86_data_t;

uint8_t l86_data_available(void);
uint8_t l86_read_rxStat(void);
parse_l86_data_t l86_get_gpsData(void);
void l86_init(void);
void l86_reboot(void);
void l86_nmea_parser(void);
void l86_start_rx(void);
void l86_rx_handler(void);
_Bool l86_rmc_selection(void);
_Bool l86_get_rmcStat(void);
_Bool l86_sar_command(const char* command, const char* reply);

#ifdef __cplusplus
}
#endif

#endif
