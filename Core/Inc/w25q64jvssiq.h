#include "main.h"

#define RELEASEPOWD_DEVID			0xAB
#define READMANUFACTURER_DEVID		0x90
#define READMANUFACTURER_DEVID_DUAL	0x92
#define READMANUFACTURER_DEVID_QUAD	0x94
#define UNIQUE_ID					0x4B
#define JEDEC_ID					0x9F

void w25q64_init(void);
